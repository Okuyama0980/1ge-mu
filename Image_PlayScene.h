#pragma once
#include "Engine/GameObject.h"

class Player;
class Hacker;
class PlayScene;
class GameManagement;

//プレイシーンで使用されるロゴを一つにまとめ管理するクラス
class Image_PlayScene : public GameObject
{
	Player* pPlayer_;			//	プレイヤークラス
	Hacker* pHacker_;			//	ハッカークラス
	PlayScene* pPlayScene_;		//　プレイシーン
	GameManagement* pGM_;		//  ゲームマネージメント

	const float FRAME_POSITION_X;	// = -0.89f; FRAME_POSITION_X = 枠の位置 X
	const float FRAME_POSITION_Y;	// = -0.81f; FRAME_POSITION_Y = 枠の位置 Y

	const float EYE_SPOSITION_X;	// = 0.71f; EYE_SPOSITION_X = 目の位置 X
	const float EYE_SPOSITION_Y;	// = 0.81f; EYE_SPOSITION_X = 目の位置 Y

	const float WATCH_POSITION_X;	// = 0.71f; WATCH_POSITION_X = 時計の位置 X

	const float NUMBER_COUNT_POSITION_X;	// = 0.89f; NUMBER_COUNT_POSITION_X = カウントの数字の位置 X
	const float NUMBER_COUNT_POSITION_Y;	// = 0.81f; NUMBER_COUNT_POSITION_Y = カウントの数字の位置 Y

	const float NUMBER_GAMETIME_MINUTES_POS_X;		// = 0.5f; NUMBER_GAMETIME_MINUTES_POS_X = ゲーム時間の1分の位の数字の位置 X
	const float NUMBER_GAMETIME_TEN_SECONDS_POS_X;	// = 0.7f; NUMBER_GAMETIME_TEN_SECONDS_POS_X = ゲーム時間の10秒の位の数字の位置 X
	const float NUMBER_GAMETIME_ONE_SECONDS_POS_X;	// = 0.9f; NUMBER_GAMETIME_ONE_SECONDS_POS_X = ゲーム時間の1秒の位の数字の位置 X


	//画像のイメージを張り付ける用

	//現在のモードを表すイメージ
	//ハッカーモード
	int			hackerImageHandle_;
	Transform	hackerTransform_;

	//エージェントモード
	int			agentImageHandle_;
	Transform	agentTransform_;

	//黒枠
	int			frameImageHandle_[2]; //[0] = 数字用(右上)　[1] = ゲーム時間用(右下)
	Transform	frameCountTransform_;	//様々なカウント用の枠
	Transform	frameGameTimeTransform_;	//ゲーム時間を表示するための枠（世界時間）

	//警戒度を表す目
	//青い目
	int			bEyeImageHandle_;
	Transform	bEyeTransform_;

	//黄色の目
	int			yEyeImageHandle_;
	Transform	yEyeTransform_;

	//赤い目
	int			rEyeImageHandle_;
	Transform	rEyeTransform_;

	//閉じている目
	//青目閉じる
	int			bEyeClose_ImageHandle_;
	Transform	bEyeCloseTransform_;

	//黄目閉じる
	int			yEyeClose_ImageHandle_;
	Transform	yEyeCloseTransform_;

	//赤目はいらない

	//時計
	int			watchImageHandle_;
	Transform	watchTransform_;

	//colon
	int			colonImageHandle_;
	Transform	colonTransform_;

	//数字
	int			numberImageHandle_[11][4];	//	[数字を0〜9 + ∞を格納][0 = カウント用(右上), 1〜3 = ゲーム時間計測(右下)　1 = 分, 2 = 10秒, 3 = 1秒を表す]
	Transform	numberCountTransform_;				//様々なカウント用
	Transform	numberGameTimeMinutesTransform_;		//ゲーム時間の分
	Transform	numberGameTimeTenSecondsTransform_;	//ゲーム時間の秒の10の位
	Transform	numberGameTimeOneSecondsTransform_;	//ゲーム時間の秒の1の位

public:
	//コンストラクタ
	Image_PlayScene(GameObject* parent);

	//デストラクタ
	~Image_PlayScene();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};