#include "BackGround_Sky.h"
#include "Engine/Image.h"



//コンストラクタ
BackGround_Sky::BackGround_Sky(GameObject* parent)
	:GameObject(parent, "BackGround_Sky"),
/*変数*/
	skyImageHandle_(-1), skyTransform_(Transform())


{
}

//デストラクタ
BackGround_Sky::~BackGround_Sky()
{
}

//初期化
void BackGround_Sky::Initialize()
{
	//画像データのロード
	//背景　空
	skyImageHandle_ = Image::Load("PNG\\Sky.png");
	assert(skyImageHandle_ >= 0);
	skyTransform_.position_ = XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);
	Image::SetTransform(skyImageHandle_, skyTransform_);

}

//更新
void BackGround_Sky::Update()
{
}

//描画
void BackGround_Sky::Draw()
{
	Image::Draw(skyImageHandle_); //背景　空
}

//開放
void BackGround_Sky::Release()
{
	skyImageHandle_ = -1;	//背景　空
}