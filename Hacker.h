#pragma once
#include "Engine/GameObject.h"

#define AGENTMODE 1		// = 1 AGENTMODE = エージェントモード
#define HACKERMODE 2	// = 2 HACKERMODE = ハッカーモード

//前方宣言		ポインタだけなら作れる　
//				ステージクラスの中身がわからないといけないものは作れない
class Stage;
class Player;


//ハッカー,ハッカーモードを管理するクラス
class Hacker : public GameObject
{

	Player* pPlayer_;

	//もしかしたらモデルいらないかも
	//int hModel_;    //モデル番号

	XMVECTOR PlayerPos_;	//プレイヤーのポジションを格納

	const int VIEW_TIME;	// = 3; VIEW_TIME	= 3秒の見るための時間を定める定数

	bool PosFlg_;		//位置を初期化するフラグ


	/*関数*/

	//ハッカーキャラ(ドローン)の移動
	//引数　なし　
	//戻値　なし
	void Hacker_HackerMove();

	//ハッカー(ドローン)視点のカメラ
	//引数　移動ベクトル　と　行列　
	//戻値　なし
	//matX,matY : 行列　作成方法 -> XMMATRIX　○○
	void Hacker_HackerCamera(XMMATRIX matX, XMMATRIX matY);

public:
	//イメージで時間計測を表示するために使いたい
	int viewTime_;	//　見ている時間が格納される変数

	//コンストラクタ
	Hacker(GameObject* parent);

	//デストラクタ
	~Hacker();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};