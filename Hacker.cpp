#include <time.h>					//時間

#include "Hacker.h"

#include "Stage.h"					//ステージクラス　レイ用 判定用
#include "RoutePoint.h"
#include "Player.h"					//プレイヤーの位置情報のため

#include "Engine/Model.h"			//モデル
#include "Engine/Input.h"			//キー　マウス操作のため
#include "Engine/Direct3D.h"
#include "Engine/Camera.h"			//カメラ　レイ用　画面のカメラではない


//コンストラクタ
Hacker::Hacker(GameObject* parent)
	:GameObject(parent, "Hacker"),/*, hModel_(-1)*/
/*ポインタ用*/
	pPlayer_(nullptr),
/*ベクトル*/
	PlayerPos_{ 0,0,0,0 },
/*フラグ*/
	PosFlg_(true),
/*変数*/
	viewTime_(0.0f),
/*定数*/
	VIEW_TIME(3)
	
{
}

//デストラクタ
Hacker::~Hacker()
{
}

//初期化
void Hacker::Initialize()
{
	pPlayer_ = (Player*)FindObject("Player");	//Playerオブジェクトを探す
	assert(pPlayer_ != nullptr);				//　pPlayer_ が null なわけがない！

	//最初は呼ばれたときのプレイヤーの座標をとってそこに出現させる
	PlayerPos_ = pPlayer_->Player_GetPos();
		
	transform_.position_ = PlayerPos_;	//Playerの位置を代入

}

//更新
void Hacker::Update()
{
	//Playerクラスのhackerフラグが立っている時
	if (pPlayer_->hackerFlg_) {

		//今のプレイヤーの座標に出現させる
		if (PosFlg_) {

			//プレイヤーの座標をとってそこに出現させる
			PlayerPos_ = pPlayer_->Player_GetPos();
			transform_.position_ = PlayerPos_;	//Playerの位置を代入
			PosFlg_ = false;
		}

		Hacker_HackerMove();

		//作業中の時間を取り続ける
		pPlayer_->h_End_ = GetTickCount64();

		//時間を表示するための時間取得 100で割ったら1秒の10分の1
		viewTime_ = 3 - (pPlayer_->h_End_ - pPlayer_->h_Start_) / 1000;

		//作業時間がオーバーしてる
		if ((pPlayer_->h_End_ - pPlayer_->h_Start_) / 1000 >= VIEW_TIME) {

			pPlayer_->hackerFlg_ = false;
			pPlayer_->Player_SetChangeMode(AGENTMODE);
		}
	}
	else {
		PosFlg_ = true;
	}


}

//描画
void Hacker::Draw()
{
	
}

//開放
void Hacker::Release()
{
}

//ハッカーキャラ(ドローン)の移動
void Hacker::Hacker_HackerMove()
{
	XMMATRIX matY;//回転Y軸用の行列
	XMMATRIX matX;//回転X軸用の行列
	matY = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));//Y軸で自分のY座標分回転させる行列
	matX = XMMatrixRotationX(XMConvertToRadians(transform_.rotate_.vecX));//X軸で自分のX座標分回転させる行列

	//移動速度
	XMVECTOR right{ 0.05f,0,0,0 };//左右用のベクトル
	XMVECTOR front{ 0,0,0.05f,0 };//前後用のベクトル
	right = XMVector3TransformCoord(right, matY);
	front = XMVector3TransformCoord(front, matY);

	//Wを押したら奥に移動	キーボード
	if (Input::IsKey(DIK_W))
	{
		//奥へ移動
		transform_.position_ += front;

	}
	//Sを押したら手前に移動	　キーボード
	if (Input::IsKey(DIK_S))
	{
		//後ろへ移動
		transform_.position_ -= front;

	}
	//Dを押したら右に移動　キーボード
	if (Input::IsKey(DIK_D))
	{
		//右へ移動
		transform_.position_ += right;

	}
	//Aを押したら左に移動　キーボード
	if (Input::IsKey(DIK_A))
	{
		//左へ移動
		transform_.position_ -= right;
	}

	//ハッカーカメラ
	Hacker_HackerCamera(matX, matY);
}

//ハッカーモード用のカメラ
void Hacker::Hacker_HackerCamera(XMMATRIX matX, XMMATRIX matY)
{
	//見える範囲を指定
	XMVECTOR camVec = XMVectorSet(0, 5.5, -1, 0);
	camVec = XMVector3TransformCoord(camVec, matX * matY);

	Camera::SetPosition(transform_.position_ + camVec);
	Camera::SetTarget(transform_.position_);//カメラを自分に向ける
}