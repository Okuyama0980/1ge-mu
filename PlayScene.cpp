#include<time.h>

#include "BackGround_Black.h"
#include "BackGround_Sky.h"
#include "BackGround_RollLeft.h"
#include "BackGround_RollRight.h"

#include "Image_PlayScene.h"

#include "GameManagement.h"
#include "PlayScene.h"
#include "Stage.h"
#include "Player.h"
#include "WanderEnemy.h"
#include "RoutePoint.h"
#include "Hacker.h"

#include "Engine/SceneManager.h"	//画面遷移


//コンストラクタ
PlayScene::PlayScene(GameObject* parent)
	: GameObject(parent, "PlayScene"),
/*フラグ*/
	yellowFlg_OK_(false), redFlg_OK_(false),
/*ポインタ用*/
	pGM_(nullptr)
{
}

//初期化
void PlayScene::Initialize()
{
	srand(time(NULL));//ランダムのたね

	////オブジェクト////

	//ゲームマネージメント
	Instantiate<GameManagement>(this);

	//空
	Instantiate<BackGround_Sky>(this);

	//表示
	//マップ
	Instantiate<Stage>(this);

	//プレイヤー
	Instantiate<Player>(this);

	//頭上カメラ
	Instantiate<Hacker>(this);

	//ポイント
	Instantiate<RoutePoint>(this);


	for (int i = 0; i < 5; i++) 
	{
		//敵(エネミー)
		Instantiate<WanderEnemy>(this);
	}





	//ロゴ
	Instantiate<Image_PlayScene>(this);


	

	pGM_ = (GameManagement*)FindObject("GameManagement");	//GameManagementオブジェクトを探す
	assert(pGM_ != nullptr);								//　pGM_ が null なわけがない！
}

//更新
void PlayScene::Update()
{

	//GameManagementクラスのGM_getNowMove()がYELLOWで　まだOKフラグが立っていない時
	if (pGM_->GM_getNowMove() == YELLOW && yellowFlg_OK_ == false) {
		Instantiate<WanderEnemy>(this);
		yellowFlg_OK_ = true;
	}

	//GameManagementクラスのGM_getNowMove()がREDで　まだOKフラグが立っていない時
	if (pGM_->GM_getNowMove() == RED && redFlg_OK_ == false) {
		Instantiate<WanderEnemy>(this);
		redFlg_OK_ = true;
	}



}

//描画
void PlayScene::Draw()
{
}

//開放
void PlayScene::Release()
{
}