#pragma once
#include "Engine/GameObject.h"


//前方宣言		ポインタだけなら作れる　
//				ステージクラスの中身がわからないといけないものは作れない
class Stage;
class Player;
class GameManagement;

//壁にぶつかったら右か左かを選出する

//歩き回る敵を管理するクラス　wander : 歩き回る、さまよう
class WanderEnemy : public GameObject
{
	Stage* pStage_;
	Player* pPlayer_;
	GameManagement* pGM_;

	clock_t s_Start_;		// スタート時間		プレイヤーを探す	Seek：求める
	clock_t s_End_;			// 終了時間　		プレイヤーを探す　	Seek：求める

	bool hitWallFlg;	//壁にぶつかった時に出るフラグ

	bool hitWall_ZFlg;	//1	Z壁にぶつかった時に出るフラグ
	bool hitWall_XFlg;	//2	X壁にぶつかった時に出るフラグ
	bool hitWall_nZFlg;	//3	-Z壁にぶつかった時に出るフラグ
	bool hitWall_nXFlg;	//4	-X壁にぶつかった時に出るフラグ

	bool playerSeeFlg; //プレイヤーを見た時に出るフラグ


	int hModel_;	//モデル番号

	int moveDirection_;			//動くための向きが入る変数
	int moveDirectionBefore_;	//前の向きを入れる用変数
	int moveDirectionReverse_;	//逆走防止用の変数


	const float BLUE_SPEED;		// = 0.045; BlUE_SPEED	= 警戒度青の速さ
	const float YELLOW_SPEED;	// = 0.07; YELLOW_SPEED = 警戒度黄の速さ
	const float RED_SPEED;		// = 0.09;　RED_SPEED	= 警戒度赤の速さ

	const float RADIUS;			// = 0.25 ; radius = 約半径

	//スポーン位置
	const float LOCATION_A_X;		// = 16.5f; location = 初期位置A_X
	const float LOCATION_A_Z;		// = 22.5f; location = 初期位置A_Z

	const float LOCATION_B_X;		// = 8.5; location = 初期位置B_X
	const float LOCATION_B_Z;		// = 8.5; location = 初期位置B_Z

	const float LOCATION_C_X;		// = 8.5; location = 初期位置C_X
	const float LOCATION_C_Z;		// = 8.5; location = 初期位置C_Z


	const int DIRECTION_Z;		// = 1; DIRECTION_Z		= Zを向いている
	const int DIRECTION_X;		// = 2; DIRECTION_X		= Xを向いている
	const int DIRECTION_NOTZ;	// = 3; DIRECTION_NOTZ	= -Zを向いている
	const int DIRECTION_NOTX;	// = 4; DIRECTION_NOTX	= -Xを向いている
	const int DIRECTION_OVER;	// = 5; DIRECTION_OVER	= 向いている方向がオーバーしている

	const float VISUAL_LIMIT;		//= 2.9;　 Visual Limit = 視野限界
	const int	VISUAL_RECOVERY;	// = 3;	  VISUAL_RECOVERY　= 視野回復　Visual recovery 
	const int	VIEWING_ANGLE;		// = 10;   VIEWING_ANGLE = 視野角　Viewing angle

	const XMVECTOR DEFO_FRONT;	// = { 0,0,1,0 }  DEFO_FRONT = デフォルトの向きベクトル
	const XMVECTOR DEFO_MOVE;	// = { 0,0,0,0 }　DEFO_MOVE = デフォルトの移動ベクトル




	//動きを変えるための変数
	//BLUE_MOVE		= 1
	//YELLOW_MOVE	= 2
	//RED_MOVE		= 3
	unsigned int changeMove_;

	//今のEnemyの速さを入れる変数
	//BlUE_SPEED	= 0.045f
	//YELLOW_SPEED	= 0.07f
	//RED_SPEED		= 0.09f
	float nowSpeed_;


	/*関数*/

	//Playerを探す関数
	//探すための準備をまとめている
	void Enemy_PlayerSeek();

	//視野
	//引数　viewAngle　	ラジアンを角度に直した視野角 
	//引数　viewDistance　対象までの距離
	void Enemy_VisualField(float viewAngle, float viewDistance);

	//キャラクタの向きを移動している方向に変える
	//引数　動いている方向
	//move :　移動ベクトル　作成方法 ->XMVECTOR move { 0,0,1,0 }
	void Enemy_CharTurn(XMVECTOR move);

	//一つぶつかった時の経路を探す
	//引数　なし
	//戻り値　進むべきベクトル
	int Enemy_OneHitSeekRoot();

	//二つぶつかった時の経路を探す
	//引数　なし
	//戻り値　進むべきベクトル
	int Enemy_TwoHitSeekRoot();

	//今向いている方向の逆を調べる
	//引数　今向いている方向
	//戻り値　今進んでいる方向の逆の方向
	int Enemy_DirectionReverse(int moveDirection);

	//行動パターン 警戒度青
	void Enemy_Blue_ActionPattern();

	//行動パターン 警戒度黄
	void Enemy_Yellow_ActionPattern();

	//行動パターン 警戒度赤
	void Enemy_Red_ActionPattern();

public:



	//コンストラクタ
	WanderEnemy(GameObject* parent);

	//デストラクタ
	~WanderEnemy();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//判定処理
	//引数　なし　
	//戻値　なし
	void Enemy_HitWall();

	//描画
	void Draw() override;

	//開放
	void Release() override;
	
};