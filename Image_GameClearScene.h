#pragma once
#include "Engine/GameObject.h"

//ゲームクリアシーンで使用されるロゴを一つにまとめ管理するクラス
class Image_GameClearScene : public GameObject
{
	//画像のイメージを張り付ける用
	//ゲームオーバーロゴ
	int			gameClearImageHandle_;
	Transform	gameClearTransform_;

	//コンテニューYNロゴを管理する
	int			continueYNImageHandle_;
	Transform	continueYNTransform_;

public:
	//コンストラクタ
	Image_GameClearScene(GameObject* parent);

	//デストラクタ
	~Image_GameClearScene();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};