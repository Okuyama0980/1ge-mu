#include "BackGround_RollLeft.h"
#include "Engine/Image.h"

//コンストラクタ
BackGround_RollLeft::BackGround_RollLeft(GameObject * parent)
	:GameObject(parent, "BackGround_RollLeft"),
/*変数*/
	rollLeftImageHandle_(-1),
/*定数*/
	DEFO_POSX(0.6f), DRAW_POSX(3.79f), OVER_POSX(-2.6f), SPEED(0.01f) 
{
}

//デストラクタ
BackGround_RollLeft::~BackGround_RollLeft()
{
}

//初期化
void BackGround_RollLeft::Initialize()
{
	//画像データのロード
	rollLeftImageHandle_ = Image::Load("PNG\\BackRoll2.png");
	assert(rollLeftImageHandle_ >= 0);
	SetPosition(XMVectorSet(DEFO_POSX, 0.2, 0, 0));


}

//更新
void BackGround_RollLeft::Update()
{

	transform_.position_.vecX -= SPEED;//移動

	//再度表示したい
	//Xがある一定値を超えたら
	if (transform_.position_.vecX < OVER_POSX)
	{
		//もう一度描画する位置を定めて描画
		transform_.position_.vecX = DRAW_POSX;
		Image::SetTransform(rollLeftImageHandle_, transform_);
		Image::Draw(rollLeftImageHandle_);


	}
}

//描画
void BackGround_RollLeft::Draw()
{
	Image::SetTransform(rollLeftImageHandle_, transform_);
	Image::Draw(rollLeftImageHandle_);


}

//開放
void BackGround_RollLeft::Release()
{
	rollLeftImageHandle_ = -1;
}