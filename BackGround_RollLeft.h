#pragma once
#include "Engine/GameObject.h"

//回転する背景左を管理するクラス
class BackGround_RollLeft : public GameObject
{
	//画像のイメージを張り付ける用
	int rollLeftImageHandle_;    //画像番号

	const float DEFO_POSX;	// =  3.8f　DEFO_POSX = デフォルトのX位置
	const float DRAW_POSX;	// =  3.79f　DEFO_POSX = 描画するX位置
	const float OVER_POSX;	// =  -2.6f　OVER_POSX = 超えたら再度描画するための規定値X位置

	const float SPEED;		// = 0.01; speed = 画面を流れる速さ

public:
	//コンストラクタ
	BackGround_RollLeft(GameObject* parent);

	//デストラクタ
	~BackGround_RollLeft();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};