#pragma once
#include "Engine/GameObject.h"

//背景：空を管理するクラス
class BackGround_Sky : public GameObject
{

	//画像のイメージを張り付ける用
	int			skyImageHandle_; //背景　空
	Transform	skyTransform_;	//Transform	skyTransform_ = Transform()


public:
	//コンストラクタ
	BackGround_Sky(GameObject* parent);

	//デストラクタ
	~BackGround_Sky();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};