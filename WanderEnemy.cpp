#include <stdlib.h>   // rand, srand
#include <time.h>     

#include "WanderEnemy.h"
#include "Stage.h"
#include "Player.h"
#include "GameManagement.h"

#include "Engine/Model.h"
#include "Engine/SphereCollider.h"	//コライダー（衝突範囲）


//複数隊出現させるときは同じ属性をもつ物が生成されるので一体で処理しても他が処理されていない
//なので、全体の統一化を図るため情報はゲームマネージャーに渡す


//コンストラクタ
WanderEnemy::WanderEnemy(GameObject * parent)
	:GameObject(parent, "WanderEnemy"), 
/*変数*/	
		hModel_(-1), changeMove_(BLUE), nowSpeed_(0), moveDirection_(0), 
		moveDirectionBefore_(0), moveDirectionReverse_(0), s_Start_(0), s_End_(0),
/*フラグ*/
		hitWallFlg(false), playerSeeFlg(false), hitWall_ZFlg(false), hitWall_nZFlg(false), hitWall_XFlg(false), hitWall_nXFlg(false),
/*ポインタ用*/
		pStage_(nullptr), pPlayer_(nullptr), pGM_(nullptr),
/*定数*/	
		RADIUS(0.25f), 
		
		LOCATION_A_X(16.5f), LOCATION_A_Z(22.5f), 
		LOCATION_B_X(17.5f), LOCATION_B_Z(5.5f),
		LOCATION_C_X(10.5f), LOCATION_C_Z(19.5f),
		
		DEFO_FRONT{ 0,0,1,0 }, DEFO_MOVE{ 0,0,0,0 },
		DIRECTION_Z(1), DIRECTION_X(2), DIRECTION_NOTZ(3), DIRECTION_NOTX(4), DIRECTION_OVER(5), VISUAL_LIMIT(2.9), VISUAL_RECOVERY(3), VIEWING_ANGLE(10),
/*Enemy行動定数*/	
		BLUE_SPEED(0.045f), YELLOW_SPEED(0.07f), RED_SPEED(0.09f)
			
			
{
}

//デストラクタ
WanderEnemy::~WanderEnemy()
{
}

//初期化
void WanderEnemy::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("NewEnemy.fbx");
	assert(hModel_ >= 0);

	//向きを決める
	moveDirection_ = rand() % 4 + 1;


	//スポーン
	int spawn = rand() % 3 + 1;
	switch (spawn)
	{
	case 1:
		transform_.position_.vecX = LOCATION_A_X;
		transform_.position_.vecZ = LOCATION_A_Z;
		break;

	case 2:

		//x-17z-5
		transform_.position_.vecX = 17.5f;
		transform_.position_.vecZ = 5.5f;

		break;

	case 3:

		//x-10z-19
		transform_.position_.vecX = 10.5f;
		transform_.position_.vecZ = 19.5f;

		break;

	//何かあった時のため
	default:
		break;
	}
	







	//コライダー（衝突範囲）準備
	SphereCollider* collision = new SphereCollider(XMVectorSet(0, 0, 0, 0), 0.3f);
	AddCollider(collision);

	pStage_ = (Stage*)FindObject("Stage");	//ステージオブジェクトを探す
	assert(pStage_ != nullptr);				//　pStage が null なわけがない！

	pPlayer_ = (Player*)FindObject("Player");	//Playerオブジェクトを探す
	assert(pPlayer_ != nullptr);				//　pPlayer_ が null なわけがない！

	pGM_ = (GameManagement*)FindObject("GameManagement");	//GameManagementオブジェクトを探す
	assert(pGM_ != nullptr);								//　pGM_ が null なわけがない！

}

//更新
void WanderEnemy::Update()
{

	//前の向きを入れる
	moveDirectionBefore_ = moveDirection_;

	//ゲームの進行に合わせて全敵の動きが変わるようにする
	switch (pGM_->GM_getNowMove())
	{
	//警戒度青の時
	case BLUE:

		//既定のアクション
		Enemy_Blue_ActionPattern();
		break;

	//警戒度黄の時
	case YELLOW:

		//既定のアクション
		Enemy_Yellow_ActionPattern();
		break;

	//警戒度赤の時
	case RED:

		//既定のアクション
		Enemy_Red_ActionPattern();
		break;

	//何かあった時のため
	default:
		break;
	}


	//当たり判定
	Enemy_HitWall();

	if (hitWallFlg == true) {

		//逆走防止
		moveDirectionReverse_ = Enemy_DirectionReverse(moveDirectionBefore_);


		//抽選した向きが前と同じ または　逆走と同じ間再抽選
		while (moveDirectionBefore_ == moveDirection_ || moveDirectionReverse_ == moveDirection_) {
			moveDirection_ = rand() % 4 + 1; //0~3 + 1

		};


		//絶対にフラグが一つしか立っていない時
		if (hitWall_ZFlg == true  && hitWall_XFlg == false && hitWall_nZFlg == false && hitWall_nXFlg == false ||
			hitWall_ZFlg == false && hitWall_XFlg == true  && hitWall_nZFlg == false && hitWall_nXFlg == false ||
			hitWall_ZFlg == false && hitWall_XFlg == false && hitWall_nZFlg == true  && hitWall_nXFlg == false ||
			hitWall_ZFlg == false && hitWall_XFlg == false && hitWall_nZFlg == false && hitWall_nXFlg == true) {

		//一つの壁にぶつかった時のルートを探す
		moveDirection_ = Enemy_OneHitSeekRoot();

		}

		//ありえない組み合わせ
		//奥と手前が同時に判定されてるとき
		if(hitWall_ZFlg == true && hitWall_nZFlg == true)
			//フラグをやり直す
			hitWall_ZFlg = hitWall_nZFlg = false;

		//奥と手前が同時に判定されてるとき
		if (hitWall_XFlg == true && hitWall_nXFlg == true)
			//フラグをやり直す
			hitWall_XFlg = hitWall_nXFlg = false;



		//特定のフラグが立っているとき
		if (hitWall_ZFlg == true  && hitWall_XFlg == true  && hitWall_nZFlg == false && hitWall_nXFlg == false || //奥と右
			hitWall_ZFlg == true  && hitWall_XFlg == false && hitWall_nZFlg == false && hitWall_nXFlg == true  || //奥と左
			hitWall_ZFlg == false && hitWall_XFlg == true  && hitWall_nZFlg == true  && hitWall_nXFlg == false || //手前と右
			hitWall_ZFlg == false && hitWall_XFlg == false && hitWall_nZFlg == true  && hitWall_nXFlg == true) { //手前の左

			//二つぶつかった時のルートを探す
			moveDirection_ = Enemy_TwoHitSeekRoot();


			//当たった壁フラグを折る
			hitWall_ZFlg = hitWall_XFlg = hitWall_nZFlg = hitWall_nXFlg = false;

		}


		//壁に当たったフラグを折る
			hitWallFlg = false;
	}

	

}

//当たり判定処理
void WanderEnemy::Enemy_HitWall()
{
	//円の円周の右、左、奥、手前の判定をつける

	int checkX, checkZ; //チェック用変数

	//左の衝突判定
	//intにキャストする　小数点切り捨て
	checkX = (int)(transform_.position_.vecX - RADIUS);
	checkZ = (int)transform_.position_.vecZ;


	if (pStage_->Stage_IsWall(checkX, checkZ))
	{
		//壁に当たらないポジションに戻す
		transform_.position_.vecX = checkX + 1.2f + RADIUS; //+ 1.2f //prevposition.vecX;

		//当たったフラグを立てる
		hitWallFlg = true;
		//当たった壁のフラグを立てる
		hitWall_nXFlg = true;

	}

	//右の衝突判定
	checkX = (int)(transform_.position_.vecX + RADIUS);
	checkZ = (int)transform_.position_.vecZ;

	if (pStage_->Stage_IsWall(checkX, checkZ))
	{
		//壁に当たらないポジションに戻す
		transform_.position_.vecX = checkX - 0.25f - RADIUS; // - 0.2f

		//当たったフラグを立てる
		hitWallFlg = true;
		//当たった壁のフラグを立てる
		hitWall_XFlg = true;
	}

	//奥の衝突判定
	checkX = (int)transform_.position_.vecX;
	checkZ = (int)(transform_.position_.vecZ + RADIUS);

	if (pStage_->Stage_IsWall(checkX, checkZ))
	{
		//壁に当たらないポジションに戻す
		transform_.position_.vecZ = checkZ - 0.25f - RADIUS;//- 0.2f

		//当たったフラグを立てる
		hitWallFlg = true;
		//当たった壁のフラグを立てる
		hitWall_ZFlg = true;
	}

	//手前の衝突判定
	checkX = (int)transform_.position_.vecX;
	checkZ = (int)(transform_.position_.vecZ - RADIUS);

	if (pStage_->Stage_IsWall(checkX, checkZ))
	{
		//壁に当たらないポジションに戻す
		transform_.position_.vecZ = checkZ + 1.2f + RADIUS; //+1.2f

		//当たったフラグを立てる
		hitWallFlg = true;
		//当たった壁のフラグを立てる
		hitWall_nZFlg = true;
	}

}

//描画
void WanderEnemy::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void WanderEnemy::Release()
{
}

//行動パターン 警戒度青
void WanderEnemy::Enemy_Blue_ActionPattern()
{	

	//今のスピードに現在の行動パターンの速さを入れる
	//移動スピードはあまり早くない　BlUE_SPEED
	nowSpeed_ = BLUE_SPEED;

	XMVECTOR move = DEFO_MOVE;	//移動ベクトル デフォルトの値で毎回初期化
	XMMATRIX EnemyCamera;	//敵の視点を弄るための行列

	//向きのいろいろ
	EnemyCamera = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
	move = XMVector3TransformCoord(move, EnemyCamera);//　EnemyCameraに正面のベクトルが入っている

	//進ませる方向
	switch (moveDirection_)
	{
	case 1:	move.vecZ = 1;	break;//奥

	case 2: move.vecX = 1;	break;//右

	case 3: move.vecZ = -1;	break;//手前

	case 4: move.vecX = -1;	break;//左

	//保険 
	default: break;
	}

	//長さを1にしないと正しい値が出ないので正規化　単位ベクトルにする 進もうとしている方向ベクトル
	move = XMVector3Normalize(move);

	//半径分(0.5)進んでしまうと他の衝突判定も拾ってしまうのでNG
	transform_.position_ += move * nowSpeed_;//移動 動く速さはnowSpeedで調整

	//向いている方向を調べ、向かせる
	Enemy_CharTurn(move);

	//プレイヤーを探す
	Enemy_PlayerSeek();



	//見つかった！　
	if (playerSeeFlg == true) 
	{

		//見つけた時間が確定してから更新
		s_End_ = GetTickCount64();


		//視野回復の3秒たった
		if ((s_End_ - s_Start_) / 1000 >= VISUAL_RECOVERY) {

			//目を開かせる
			pGM_->GM_OpenClose_Eyes();

			//見つけたフラグを戻す
			playerSeeFlg = false;
		}

	}
}

//行動パターン 警戒度黄
void WanderEnemy::Enemy_Yellow_ActionPattern()
{	

	//今のスピードに現在の行動パターンの速さを入れる
	//移動スピードがプレイヤーと同じスピードになる  YELLOW_SPEED
	nowSpeed_ = YELLOW_SPEED;

	XMVECTOR move = DEFO_MOVE;	//移動ベクトル デフォルトの値で毎回初期化
	XMMATRIX EnemyCamera;	//敵の視点を弄るための行列

	//向きのいろいろ
	EnemyCamera = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
	move = XMVector3TransformCoord(move, EnemyCamera);//　EnemyCameraに正面のベクトルが入っている

	//進ませる方向
	switch (moveDirection_)
	{
	case 1:	move.vecZ = 1;	break;//奥

	case 2: move.vecX = 1;	break;//右

	case 3: move.vecZ = -1;	break;//手前

	case 4: move.vecX = -1;	break;//左

	//保険 
	default: break;
	}

	//長さを1にしないと正しい値が出ないので正規化　単位ベクトルにする 進もうとしている方向ベクトル
	move = XMVector3Normalize(move);

	//半径分(0.5)進んでしまうと他の衝突判定も拾ってしまうのでNG
	transform_.position_ += move * nowSpeed_;//移動 動く速さはnowSpeedで調整

	//向いている方向を調べ、向かせる
	Enemy_CharTurn(move);

	//プレイヤーを探す
	Enemy_PlayerSeek();


	//見つかっている
	if (playerSeeFlg == true) 
	{
		
		//見つけた時間が確定してから更新
		s_End_ = GetTickCount64();


		//視野回復の3秒たった
		if ((s_End_ - s_Start_) / 1000 >= VISUAL_RECOVERY) {

			//目を開かせる
			pGM_->GM_OpenClose_Eyes();

			//見つけたフラグを戻す
			playerSeeFlg = false;

		}
	}
	

	
}

//行動パターン 警戒度赤
void WanderEnemy::Enemy_Red_ActionPattern()
{	
	//今のスピードに現在の行動パターンの速さを入れる
	//・移動スピードがプレイヤーよりも早くする	RED_SPEED
	nowSpeed_ = RED_SPEED;

	XMVECTOR move = DEFO_MOVE;	//移動ベクトル デフォルトの値で毎回初期化
	XMMATRIX EnemyCamera;	//敵の視点を弄るための行列

	//向きのいろいろ
	EnemyCamera = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
	move = XMVector3TransformCoord(move, EnemyCamera);//　EnemyCameraに正面のベクトルが入っている


	//進ませる方向
	switch (moveDirection_)
	{
	case 1:	move.vecZ = 1;	break;//奥

	case 2: move.vecX = 1;	break;//右

	case 3: move.vecZ = -1;	break;//手前

	case 4: move.vecX = -1;	break;//左

	//保険 
	default: break;
	}

	//長さを1にしないと正しい値が出ないので正規化　単位ベクトルにする 進もうとしている方向ベクトル
	move = XMVector3Normalize(move);

	//半径分(0.5)進んでしまうと他の衝突判定も拾ってしまうのでNG
	transform_.position_ += move * nowSpeed_;//移動 動く速さはnowSpeedで調整

	//向いている方向を調べ、向かせる
	Enemy_CharTurn(move);

	//プレイヤーを探す
	Enemy_PlayerSeek();

}

//キャラクタを動いている方向に変える
void WanderEnemy::Enemy_CharTurn(XMVECTOR move)
{
	//プレイヤーをデフォルトの向きで毎回初期化
	XMVECTOR front = DEFO_FRONT;

	//常にベクトルが1とは限らないので正規化する
	front = XMVector3Normalize(front);

	//向いている方向と動いている方向から内積を求める
	float dot = XMVector3Dot(front, move).vecX;	 //XMVector3Dot 内積 
	float angle = acos(dot);//ラジアンで求める　acosを使えば角度を求める

	//動いている方向に向くようにする変数
	XMVECTOR faceingMove = XMVector3Cross(front, move); //XMVector3Crossの引数：3Dベクトルと3Dベクトル　戻り値：二つのの外積を返す

	//左を向いている
	if (faceingMove.vecY < 0)
	{
		//左向きにするために角度-にする
		angle = -angle;
	}

	//ラジアンを度数に直してモデルが動いている方向に向くようにする
	transform_.rotate_.vecY = XMConvertToDegrees(angle); //XMConvertToDegreesの引数：ラジアン単位の角度のサイズ　戻り値：角度のサイズ（度単位）

	

}

//一つぶつかった時の経路を探す
int WanderEnemy::Enemy_OneHitSeekRoot()
{
	//手前と奥のどっちかが衝突
	if (hitWall_ZFlg == true || hitWall_nZFlg == true) {
		int move = rand() % 2 + 1; //1と2が出る　右左を出す

		//逆走防止
		//moveDirectionReverse = Enemy_DirectionReverse(moveDirectionBefore);

		////2と4を返す
		if (move == 2) 
			return moveDirection_ = DIRECTION_NOTX;
		else
			return moveDirection_ = DIRECTION_X;

	}

		//右と左のどっちかが衝突
	if (hitWall_XFlg == true || hitWall_nXFlg == true) {
		int move = rand() % 2 + 1; //1と2が出る　右左を出す

		//逆走防止
		//moveDirectionReverse = Enemy_DirectionReverse(moveDirectionBefore);

		//1と3を返す
		if (move == 2)
			return moveDirection_ = DIRECTION_Z;
		else
			return moveDirection_ = DIRECTION_NOTZ;

	}
	return 0;
}


//二つぶつかった時の経路を探す
int WanderEnemy::Enemy_TwoHitSeekRoot()
{

	//奥と右がぶつかってる時
	if (hitWall_ZFlg == true && hitWall_XFlg == true) {

		moveDirection_ = rand() % 2 + 3; //0~1 + 3 // 3と4が出る


		//逆走防止
		moveDirectionReverse_ = Enemy_DirectionReverse(moveDirectionBefore_);

		//逆走と同じ間再抽選
		while (moveDirectionReverse_ == moveDirection_) {
			moveDirection_ = rand() % 2 + 3; //0~1 + 3 // 3と4が出る

		};

		return moveDirection_;

	}

	//奥と左がぶつかってる時
	if (hitWall_ZFlg == true && hitWall_nXFlg == true) {


		moveDirection_ = rand() % 2 + 2; //0~1 + 2 // 2と3が出る

		//逆走防止
		moveDirectionReverse_ = Enemy_DirectionReverse(moveDirectionBefore_);

		//逆走と同じ間再抽選
		while ( moveDirectionReverse_ == moveDirection_) {
			moveDirection_ = rand() % 2 + 2; //0~1 + 2 // 2と3が出る


		};
			return moveDirection_;

	}


	//手前と右がぶつかってる時
	if (hitWall_nZFlg == true && hitWall_XFlg == true) {
		

		 moveDirection_ = rand() % 2 + 1; //0~1 + 1 // 1と4が出なきゃ
		 //2が出たときは2を足して4を返す
		 if (moveDirection_ == 2)
			 moveDirection_ += moveDirection_;//2+2
	
		//逆走防止
		moveDirectionReverse_ = Enemy_DirectionReverse(moveDirectionBefore_);

		//逆走と同じ間再抽選
		while (moveDirectionReverse_ == moveDirection_) {

			moveDirection_ = rand() % 2 + 1; //0~1 + 1 // 1と4が出なきゃ
		 //2が出たときは2を足して4を返す
			if (moveDirection_ == 2)
				moveDirection_ += moveDirection_;//2+2


		};
			return moveDirection_;
	}

	//手前と左がぶつかってる時
	if (hitWall_nZFlg == true && hitWall_nXFlg == true) {


		moveDirection_ = rand() % 2 + 1; //0~1 + 1 // 1と2が出る

		//逆走防止
		moveDirectionReverse_ = Enemy_DirectionReverse(moveDirectionBefore_);

		//逆走と同じ間再抽選
		while (moveDirectionReverse_ == moveDirection_) {
			moveDirection_ = rand() % 2 + 1; //0~1 + 1 // 1と2が出る


		};
		return moveDirection_;

	}


	return 0;
}

//逆走を調べる
int WanderEnemy::Enemy_DirectionReverse(int moveDirection)
{
	//逆走防止
	moveDirectionReverse_ = moveDirection + 2;

	//増えすぎた時はオーバー分を取り除く
	if (moveDirectionReverse_ == DIRECTION_OVER)
		moveDirectionReverse_ = DIRECTION_Z;//1=5-4

	if (moveDirectionReverse_ == DIRECTION_OVER + 1)
		moveDirectionReverse_ = DIRECTION_X;//2=6-4

	return moveDirectionReverse_;
}


//プレイヤーを探す
void WanderEnemy::Enemy_PlayerSeek()
{
	//Enemyの準備
	XMVECTOR Direction = { 0,0,1,0 }; //敵(Enemy)位置から Zへベクトルを飛ばす

	XMMATRIX aY;
	aY = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY)); //Y軸で○○°回転させる行列
	Direction = XMVector3TransformCoord(Direction, aY);


	//視野角＆視野距離
	XMVECTOR playerPosition = pPlayer_->Player_GetPos();//世界の中心から見たプレイヤー位置ベクトル

	XMVECTOR myPosition = transform_.position_; //世界の中心から見たEnemyの位置ベクトル つまり自分の位置


	//計算する　二つの座標の距離
	//２つの位置ベクトルの引き算した物の長さを求めれば、それは「距離」になる。
	//ａ−ｂをすると、ｂからａに向かうベクトルが求められる。
	//プレイヤ-敵をすると、敵からプレイヤに向かうベクトルが求められる。
	//なので、EnemyからPlayerにベクトルを飛ばす、レイのようなベクトルが作れる

	XMVECTOR view = playerPosition - myPosition; //敵から見たプレイヤーの方向ベクトル
	
	float viewDistance = XMVector3Length(view).vecX;//ベクトルの長さを求める つまり距離



	XMVECTOR sPlayerPosition = XMVector3Normalize(view);   //敵から見たプレイヤーの方向を正規化
	XMVECTOR sDirection = XMVector3Normalize(Direction);   //敵の向いている方向を正規化

	float dot = XMVector3Dot(sPlayerPosition, sDirection).vecX;//内積を求める
	float angle = acos(dot); //ラジアン度が求められる

	float viewAngle = XMConvertToDegrees(angle);//ラジアンを角度に直した視野角


	//視野
	Enemy_VisualField(viewAngle, viewDistance);
	
}

void WanderEnemy::Enemy_VisualField(float viewAngle, float viewDistance)
{
	//見つかる時と見つからない時がある？
	//敵すべてに視野が追加されてない？　特定の敵のみ視野がある？

	//EnemyがPlayerへ飛ばすベクトルとEnemyがZに飛ばしているベクトルの内積が VIEWINGANGLE°以下　　
	if (viewAngle <= VIEWING_ANGLE)
	{
		//EnemyとPlayerの距離が2.9メートルこれで約3マス以上は見えない 
		if (viewDistance <= VISUAL_LIMIT)
		{
			//フラグが立っていない時
			if (playerSeeFlg == false) {

				//見つけたフラグを立てる
				playerSeeFlg = true; // <[ 見つけた！]

				//プレイヤー発見！
				pGM_->GM_PlayerSeeCount();

				//一度目を閉じる！
				pGM_->GM_OpenClose_Eyes();

				s_Start_ = GetTickCount64(); //見つけた時間を取得

			}
		}
	}
}
