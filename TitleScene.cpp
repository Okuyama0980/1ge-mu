#include "TitleScene.h"

#include "Engine/SceneManager.h"
#include "Engine/Input.h"

#include "BackGround_Black.h"
#include "BackGround_RollLeft.h"
#include "BackGround_RollRight.h"

#include "Image_TitleScene.h"


//コンストラクタ
TitleScene::TitleScene(GameObject * parent)
	: GameObject(parent, "TitleScene")
{
}

//初期化
void TitleScene::Initialize()
{
	//上から順に描画

	Instantiate<BackGround_Black>(this);
	Instantiate<BackGround_RollLeft>(this);
	Instantiate<BackGround_RollRight>(this);

	Instantiate<Image_TitleScene>(this);
	
}

//更新
void TitleScene::Update()
{
	//エンターキーが押されていたら
	if (Input::IsKey(DIK_RETURN))
	{
		
		//画面遷移
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_PLAY);
	}
}

//描画
void TitleScene::Draw()
{
	
}

//開放
void TitleScene::Release()
{
	
}