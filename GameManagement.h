#pragma once
#include "Engine/GameObject.h"


//ゲームを運営するクラス（司令塔）
class GameManagement : public GameObject
{


    //=====ゲーム内の時間を管理======//

    //二つに分けた方が細かい時間指定でゲームを終了させやすいのでGAME_ENDTIMEが活用できる
    clock_t g_Start_;		// スタート時間		ゲームをする時間の縛るため
    clock_t g_End_;			// 終了時間　		ゲームをする時間の縛るため
    clock_t g_MiddleStart_; // 途中スタート時間	ゲームをする時間を測り画面に表示するため
    clock_t g_MiddleEnd_;	// 途中終了時間　	ゲームをする時間を測り画面に表示するため

    const unsigned int GAME_END_TIME;	//ゲームタイム　7分=420秒　この定数を変えればゲームオーバーの時間は変わる
    const unsigned int GAME_TIME;		//ゲーム時間を正常に減らすための定数

    //イメージで時間計測を表示するために使いたい

    int gameTimeOneSeconds_;	//　ゲームをしている時間(1秒の位)が格納される変数
    int gameTimeTenSeconds_;	//　ゲームをしている時間(10秒の位)が格納される変数
    int gameTimeMinutes_;	    //　ゲームをしている時間(1分の位)が格納される変数

    int gameTime_[3]; //格納された変数をまとめて渡す用の配列

    //ゲーム時間を計測する関数
    void GM_GameMeasurementTime();




    //=====Enemyの警戒度を管理=====//
    
    unsigned int changeMove_;       //動きを変えるための変数
    unsigned int playerSeeCount_;   //見つかった回数を格納する変数

    //警戒度を切り替える用の定数
    const int MAX_FOUND_BLUE;    // = 3; MAX_FOUND_BLUE   = 警戒度青で見つかって良い最大値
    const int MAX_FOUND_YELLOW;  // = 2; MAX_FOUND_YELLOW = 警戒度黄色で見つかって良い最大値
    const int INFINITE_COUNT; // = 10 INFINITE_COUNT = ∞を表示させるための定数
   
    //Enemyの色(警戒度)を設定する関数
    void GM_EnemySetColor();
    
    //目を閉じるかのフラグ
    bool CloseEyesFlg;







public:
    //コンストラクタ
    GameManagement(GameObject* parent);

    //デストラクタ
    ~GameManagement();

    //初期化
    void Initialize() override;

    //更新
    void Update() override;

    //描画
    void Draw() override;

    //開放
    void Release() override;


    /*=====イメージに送るためのセッターゲッターの管理=====*/

    /*ゲーム内の時間を計測し、格納した配列を渡して管理する*/
    //引数：number　0〜2までの数字　小数点なし
    //戻値：number = 0 -> ゲーム時間の1分の単位
    //      number = 1 -> ゲーム時間の10秒の単位
    //      number = 2 -> ゲーム時間の1秒の単位
    int GM_getGameTime(int number) { return gameTime_[number]; }


    /*プレイヤーが見つかった回数を管理してプレイシーン上に存在するEnemyの警戒度を管理する*/
    //プレイヤーが発見された時呼ばれる関数
    //引数：なし
    //戻値：なし
    void GM_PlayerSeeCount() { playerSeeCount_ += 1; }

    //今現在の警戒度を渡す
    //引数：なし
    //戻値：現在の警戒度
    int GM_getNowMove() { return changeMove_; }

    //今何回見つかったを渡す
    //引数：なし
    //戻値：プレイヤーの見つかった回数
    int GM_getPlayerSeeCount() { return playerSeeCount_; }

    //目を開けたり閉じたりさせる
    //引数：なし
    //戻値：なし
    void GM_OpenClose_Eyes();

    //目UIが閉じているか渡す
    //引数：なし
    //戻値：目を閉じてるか　false　OR　true
    bool GM_getCloseEyesFlg() { return CloseEyesFlg; }
};