#pragma once
#include "Engine/GameObject.h"

//タイトルシーンで使用されるロゴを一つにまとめ管理するクラス
class Image_TitleScene : public GameObject
{
	//画像のイメージを張り付ける用
	//タイトルロゴ
	int			titleImageHandle_;	
	Transform	titleTransform_;

	//プリーズエンターロゴ
	int			pleaseImageHandle_;	
	Transform	pleaseTransform_;

public:
	//コンストラクタ
	Image_TitleScene(GameObject* parent);

	//デストラクタ
	~Image_TitleScene();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};