#include <string>

#include "Image_PlayScene.h"
#include "SceneTimer.h"
#include "Player.h"					//プレイヤーのフラグが見たい
#include "Hacker.h"					//ハッカーの見る時間が知りたい
#include "WanderEnemy.h"			//敵のフラグが見たい
#include "PlayScene.h"				//世界時間を知りたい
#include "GameManagement.h"			//ゲーム内で行われる様々なイベントの中身を知りたい

#include "Engine/Image.h"

//コンストラクタ
Image_PlayScene::Image_PlayScene(GameObject* parent)
	:GameObject(parent, "Image_PlayScene"),
	/*ポインタ用*/
	pPlayer_(nullptr), pHacker_(nullptr), pPlayScene_(nullptr), pGM_(nullptr),
	/*定数*/

	//枠
	FRAME_POSITION_X(-0.89f), FRAME_POSITION_Y(-0.81f),

	//目
	EYE_SPOSITION_X(0.71f), EYE_SPOSITION_Y(0.81f),

	//カウント数字
	NUMBER_COUNT_POSITION_X(0.89f), NUMBER_COUNT_POSITION_Y(0.81f),	

	//ゲーム時間
	NUMBER_GAMETIME_MINUTES_POS_X(0.5f),		//ゲーム時間の1分の位の数字の位置
	NUMBER_GAMETIME_TEN_SECONDS_POS_X(0.7f),	//ゲーム時間の10秒の位の数字の位置
	NUMBER_GAMETIME_ONE_SECONDS_POS_X(0.9f),	//ゲーム時間の1秒の位の数字の位置

	//時計アイコン
	WATCH_POSITION_X(0.3f),		


	/*イメージ*/
	//ハッカーモード用
	hackerImageHandle_(-1), hackerTransform_(Transform()),

	//エージェントモード用
	agentImageHandle_(-1), agentTransform_(Transform()),

	//フレーム用
	frameCountTransform_(Transform()), frameGameTimeTransform_(Transform()),

	//Eyes 青,黄色,赤
	bEyeImageHandle_(-1), bEyeTransform_(Transform()),
	yEyeImageHandle_(-1), yEyeTransform_(Transform()),
	rEyeImageHandle_(-1), rEyeTransform_(Transform()),
	//
	bEyeClose_ImageHandle_(-1), bEyeCloseTransform_(Transform()),
	yEyeClose_ImageHandle_(-1), yEyeCloseTransform_(Transform()),

	//時計
	watchImageHandle_(-1), watchTransform_(Transform()),

	//コロン
	colonImageHandle_(-1), colonTransform_(Transform()),

	//数字
	numberCountTransform_(Transform()), numberGameTimeMinutesTransform_(Transform()), 
	numberGameTimeTenSecondsTransform_(Transform()), numberGameTimeOneSecondsTransform_(Transform())

{
	//フレームの配列の初期化
	for (int i = 0; i < 2; i++) {
		frameImageHandle_[i] = -1;
	}

	//数の配列初期化
	for (int i = 0; i < 11; i++) {
		for (int j = 0; j < 4; j++) {
			numberImageHandle_[i][j] = -1;
		}
	}
	

}

//デストラクタ
Image_PlayScene::~Image_PlayScene()
{
}

//初期化
void Image_PlayScene::Initialize()
{
	//Playerオブジェクトを探す
	pPlayer_ = (Player*)FindObject("Player");	
	assert(pPlayer_ != nullptr);	//見つけられているか

	//Hackerオブジェクトを探す
	pHacker_ = (Hacker*)FindObject("Hacker");
	assert(pHacker_ != nullptr);
						
	//プレイシーンを探す
	pPlayScene_ = (PlayScene*)FindObject("PlayScene");
	assert(pPlayScene_ != nullptr);

	//ゲームマネージメントを探す
	pGM_ = (GameManagement*)FindObject("GameManagement");
	assert(pGM_ != nullptr);

	/////////////////////////////////////////////////////

	//フレーム　数字用枠
	frameImageHandle_[0] = Image::Load("PNG\\frame.png");
	assert(frameImageHandle_[0] >= 0);
	frameCountTransform_.position_ = XMVectorSet(NUMBER_COUNT_POSITION_X, NUMBER_COUNT_POSITION_Y, 0.0f, 0.0f);
	Image::SetTransform(frameImageHandle_[0], frameCountTransform_);

	//フレーム　ゲーム時間表示用
	frameImageHandle_[1] = Image::Load("PNG\\frame4.png");
	assert(frameImageHandle_[1] >= 0);
	frameGameTimeTransform_.position_ = XMVectorSet(0.6f, FRAME_POSITION_Y, 0.0f, 0.0f);
	Image::SetTransform(frameImageHandle_[1], frameGameTimeTransform_);

	/////////////////////////////////////////////////////

	//ハッカーモード
	hackerImageHandle_ = Image::Load("PNG\\Hk.png");
	assert(hackerImageHandle_ >= 0);
	hackerTransform_.position_ = XMVectorSet( FRAME_POSITION_X, FRAME_POSITION_Y, 0.0f, 0.0f);
	Image::SetTransform(hackerImageHandle_, hackerTransform_);

	//エージェントモード
	agentImageHandle_ = Image::Load("PNG\\Ag.png");
	assert(agentImageHandle_ >= 0);
	agentTransform_.position_ = XMVectorSet( FRAME_POSITION_X, FRAME_POSITION_Y, 0.0f, 0.0f);
	Image::SetTransform(agentImageHandle_, agentTransform_);


	/////////////////////////////////////////////////////
	//青い目
	bEyeImageHandle_ = Image::Load("PNG\\Eye\\BEye.png");
	assert(bEyeImageHandle_ >= 0);
	bEyeTransform_.position_ = XMVectorSet(EYE_SPOSITION_X, EYE_SPOSITION_Y, 0.0f, 0.0f);
	Image::SetTransform(bEyeImageHandle_, bEyeTransform_);

	//黄色い目
	yEyeImageHandle_ = Image::Load("PNG\\Eye\\YEye.png");
	assert(yEyeImageHandle_ >= 0);
	yEyeTransform_.position_ = XMVectorSet(EYE_SPOSITION_X, EYE_SPOSITION_Y, 0.0f, 0.0f);
	Image::SetTransform(yEyeImageHandle_, yEyeTransform_);

	//赤い目
	rEyeImageHandle_ = Image::Load("PNG\\Eye\\REye.png");
	assert(rEyeImageHandle_ >= 0);
	rEyeTransform_.position_ = XMVectorSet(EYE_SPOSITION_X, EYE_SPOSITION_Y, 0.0f, 0.0f);
	Image::SetTransform(rEyeImageHandle_, rEyeTransform_);

	//青目を閉じる
	bEyeClose_ImageHandle_ = Image::Load("PNG\\Eye\\BEye_Close.png");
	assert(bEyeClose_ImageHandle_ >= 0);
	bEyeCloseTransform_.position_ = XMVectorSet(EYE_SPOSITION_X, EYE_SPOSITION_Y, 0.0f, 0.0f);
	Image::SetTransform(bEyeClose_ImageHandle_, bEyeCloseTransform_);

	//黄目を閉じる
	yEyeClose_ImageHandle_ = Image::Load("PNG\\Eye\\YEye_Close.png");
	assert(yEyeClose_ImageHandle_ >= 0);
	yEyeCloseTransform_.position_ = XMVectorSet(EYE_SPOSITION_X, EYE_SPOSITION_Y, 0.0f, 0.0f);
	Image::SetTransform(yEyeClose_ImageHandle_, yEyeCloseTransform_);

	/////////////////////////////////////////////////////

	//時計
	watchImageHandle_ = Image::Load("PNG\\watch.png");
	assert(watchImageHandle_ >= 0);
	watchTransform_.position_ = XMVectorSet(WATCH_POSITION_X, FRAME_POSITION_Y, 0.0f, 0.0f);
	Image::SetTransform(watchImageHandle_, watchTransform_);

	//コロン
	colonImageHandle_ = Image::Load("PNG\\colon.png");
	assert(colonImageHandle_ >= 0);
	colonTransform_.position_ = XMVectorSet(WATCH_POSITION_X+0.3f, FRAME_POSITION_Y, 0.0f, 0.0f);
	Image::SetTransform(colonImageHandle_, colonTransform_);


	//数字0〜9 + ∞格納
	for (int i = 0; i < 11; i++) {
		for (int j = 0; j < 4; j++) {
			//intをstringへ
			std::string str = std::to_string(i);

			//PNGの名前を組み合わせ、それぞれの配列に格納していく 
			std::string fileName = "PNG\\Number\\" + str + (std::string)(".png");
			numberImageHandle_[i][j] = Image::Load(fileName);
			assert(numberImageHandle_[i][j] >= 0);
			numberCountTransform_.position_ = XMVectorSet(NUMBER_COUNT_POSITION_X, NUMBER_COUNT_POSITION_Y, 0.0f, 0.0f);
			numberGameTimeMinutesTransform_.position_	 = XMVectorSet(NUMBER_GAMETIME_MINUTES_POS_X, FRAME_POSITION_Y, 0.0f, 0.0f);		//1分
			numberGameTimeTenSecondsTransform_.position_ = XMVectorSet(NUMBER_GAMETIME_TEN_SECONDS_POS_X, FRAME_POSITION_Y, 0.0f, 0.0f);	//10秒
			numberGameTimeOneSecondsTransform_.position_ = XMVectorSet(NUMBER_GAMETIME_ONE_SECONDS_POS_X, FRAME_POSITION_Y, 0.0f, 0.0f);	//1秒
			//それぞれの表示位置を決める
			Image::SetTransform(numberImageHandle_[i][0], numberCountTransform_);
			Image::SetTransform(numberImageHandle_[i][1], numberGameTimeMinutesTransform_);
			Image::SetTransform(numberImageHandle_[i][2], numberGameTimeTenSecondsTransform_);
			Image::SetTransform(numberImageHandle_[i][3], numberGameTimeOneSecondsTransform_);
		}
	}

	//使わないデータを初期化
	for (int j = 1; j < 4; j++) {
		numberImageHandle_[10][j] = -1; //ゲーム時間に∞はいらない
	}

	for (int i = 6; i < 10; i++) {
		numberImageHandle_[i][2] = -1; //10秒の位に6〜9はいらない
	}

	/////////////////////////////////////////////////////

}

//更新
void Image_PlayScene::Update()
{
	


}

//描画
void Image_PlayScene::Draw()
{

	//ゲーム時間表示フレーム
	Image::Draw(frameImageHandle_[1]);

	//時計
	Image::Draw(watchImageHandle_);

	//コロン
	Image::Draw(colonImageHandle_);

	//数字
	//必要な数字をゲッターで入手
	Image::Draw(numberImageHandle_[pGM_->GM_getGameTime(2)][3]);
	Image::Draw(numberImageHandle_[pGM_->GM_getGameTime(1)][2]);
	Image::Draw(numberImageHandle_[pGM_->GM_getGameTime(0)][1]);


	//表示する画像を状況によって変更
	//Playerクラスのhackerフラグが立っている時
	if (pPlayer_->hackerFlg_) {

		//ハッカーモード
		Image::Draw(hackerImageHandle_);

		//数字用フレーム
		Image::Draw(frameImageHandle_[0]);

		//数字
		Image::Draw(numberImageHandle_[pHacker_->viewTime_][0]);


	}
	else {

		//エージェントモード
		Image::Draw(agentImageHandle_);

		//数字用フレーム
		Image::Draw(frameImageHandle_[0]);

		//今警戒度赤
		if (pGM_->GM_getNowMove() == RED) {

			//赤い目
			Image::Draw(rEyeImageHandle_);



			//数字∞を表示
			Image::Draw(numberImageHandle_[10][0]);

		}
		//今警戒度黄
		else if (pGM_->GM_getNowMove() == YELLOW) {

			//目は閉じている
			if (pGM_->GM_getCloseEyesFlg() == true)
			{
				//黄目を閉じる
				Image::Draw(yEyeClose_ImageHandle_);
			}
			//目は閉じていない
			else
			{
				//黄色い目
				Image::Draw(yEyeImageHandle_);
			}

			//数字
			Image::Draw(numberImageHandle_[pGM_->GM_getPlayerSeeCount()][0]);
		}
		//それ以外
		else {

			//目は閉じている
			if (pGM_->GM_getCloseEyesFlg() == true)
			{
				//青目を閉じる
				Image::Draw(bEyeClose_ImageHandle_);
			}
			//目は閉じていない
			else
			{
				//青い目
				Image::Draw(bEyeImageHandle_);
			}

			//数字
			Image::Draw(numberImageHandle_[pGM_->GM_getPlayerSeeCount()][0]);
		}
		

	}


}

//開放　UI
void Image_PlayScene::Release()
{
	//フレーム
	for (int i = 0; i < 2; i++) {
		frameImageHandle_[i] = -1;
	}

	//エージェントモード
	agentImageHandle_ = -1;

	//ハッカーモード
	hackerImageHandle_ = -1;

	//青い目
	bEyeImageHandle_ = -1;

	//黄色い目
	yEyeImageHandle_ = -1;

	//赤い目
	rEyeImageHandle_ = -1;

	//青目を閉じる
	bEyeClose_ImageHandle_ = -1;

	//黄目を閉じる
	yEyeClose_ImageHandle_ = -1;

	//時計
	watchImageHandle_ = -1;

	//コロン
	colonImageHandle_ = -1;

	//数の配列初期化
	for (int i = 0; i < 11; i++) {
		for (int j = 0; j < 4; j++) {
			numberImageHandle_[i][j] = -1;
		}
	}
	
}