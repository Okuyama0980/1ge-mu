#include "BackGround_Black.h"
#include "Engine/Image.h"



//コンストラクタ
BackGround_Black::BackGround_Black(GameObject * parent)
	:GameObject(parent, "BackGround_Black"), 
/*変数*/
	blackImageHandle_(-1),   blackTransform_(Transform())


{
}

//デストラクタ
BackGround_Black::~BackGround_Black()
{
}

//初期化
void BackGround_Black::Initialize()
{
	//画像データのロード
	//背景　黒
	blackImageHandle_ = Image::Load("PNG\\BackGround.png");
	assert(blackImageHandle_ >= 0);
	blackTransform_.position_ = XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);
	Image::SetTransform(blackImageHandle_, blackTransform_);
	
}

//更新
void BackGround_Black::Update()
{
}

//描画
void BackGround_Black::Draw()
{
	Image::Draw(blackImageHandle_); //背景　黒
}

//開放
void BackGround_Black::Release()
{
	blackImageHandle_ = -1;	//背景　黒
}