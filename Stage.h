#pragma once
#include "Engine/GameObject.h"

//ステージを管理するクラス
class Stage : public GameObject
{
	int hModel_[3];    //モデル番号

	int stageA_[35][35];

	//Transform transform;

public:
	//RoutePointクラスでも使いたい
	const int MAP_LENGTH_X;		// = 35; MAP_LENGTH_X	= マップのXの長さ
	const int MAP_LENGTH_Y;		// = 35; MAP_LENGTH_Y	= マップのYの長さ

	//コンストラクタ
	Stage(GameObject* parent);

	//デストラクタ
	~Stage();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;


	//そこは壁か？
	//引数：座標X,Y
	//戻り値：壁　true, それ以外　false
	bool Stage_IsWall(int x, int z);

	//そこはゴールか？
	//引数：座標X,Y
	//戻り値：ゴール　true, それ以外　false
	bool Stage_IsGoal(int x, int z);

	//RoutePoint//点打ち判定処理で使う
	//床のハンドルを渡す
	int Stage_GetModelFloor() { return hModel_[0]; }




};