#include<time.h>

#include "GameManagement.h"
#include "Engine/SceneManager.h"	//画面遷移



//コンストラクタ
GameManagement::GameManagement(GameObject* parent)
    :GameObject(parent, "GameManagement"),

    //=====ゲーム内の時間を管理======//
 /*変数*/
    g_Start_(0), g_End_(0), g_MiddleStart_(0), g_MiddleEnd_(0),
    gameTimeOneSeconds_(0), gameTimeTenSeconds_(5), gameTimeMinutes_(6),

 /*定数*/
        GAME_END_TIME(420), GAME_TIME(9),



    //=====Enemyの警戒度を管理=====//

 /*変数*/
        changeMove_(1), playerSeeCount_(0), CloseEyesFlg(false),
 /*定数*/
        MAX_FOUND_BLUE(3), MAX_FOUND_YELLOW(2), INFINITE_COUNT(10)

    

{
    //回数分初期化
    for (int i = 0; i < 3; i++)
    {
        gameTime_[i] = 0;
    }

}

//デストラクタ
GameManagement::~GameManagement()
{
}

//初期化
void GameManagement::Initialize()
{
    //ゲームが始まった時の時間を取得
    g_Start_ = GetTickCount64();

    //ゲームの経過した時間を計るための時間を取得
    g_MiddleStart_ = GetTickCount64();
}

//更新
void GameManagement::Update()
{
    //ゲーム時間を計測する関数
    GM_GameMeasurementTime();

    //Enemyの色(警戒度)を設定する関数
    GM_EnemySetColor();
    



}

//描画
void GameManagement::Draw()
{
}

//開放
void GameManagement::Release()
{
}



//=====ゲーム内の時間を管理======//
//ゲーム時間を計測する関数
void GameManagement::GM_GameMeasurementTime()
{
    // ゲームの経過した時間を計るための時間を取得
    g_MiddleEnd_ = GetTickCount64();

    //ゲームが終わるため時の時間を取得
    g_End_ = GetTickCount64();

    //イメージで描画する用に
    //時間を表示するための時間取得 1000で割って1秒
    gameTimeOneSeconds_ = GAME_TIME - (g_MiddleEnd_ - g_MiddleStart_) / 1000;

    //10秒の位を管理
    if (gameTimeOneSeconds_ < 0) {

        //10の位を1減らす
        gameTimeTenSeconds_ -= 1;

        //画面がチカチカするのであらかじめ代入してしまう
        gameTimeOneSeconds_ = 9;

        //ゲームの経過した時間を初期化
        g_MiddleEnd_ = 0;
        g_MiddleStart_ = GetTickCount64();
    }

    //1分の位を管理
    if (gameTimeTenSeconds_ < 0) {

        //分の位を1減らす
        gameTimeMinutes_ -= 1;

        //10の位を5に戻す
        gameTimeTenSeconds_ = 5;

        //ゲームの経過した時間を計るため初期化
        g_MiddleEnd_ = 0;
        g_MiddleStart_ = GetTickCount64();
    }

    //渡す用の配列に格納
    gameTime_[0] = gameTimeMinutes_;    //　ゲームをしている時間(分)を格納
    gameTime_[1] = gameTimeTenSeconds_; //  ゲームをしている時間(10秒)を格納
    gameTime_[2] = gameTimeOneSeconds_; //  ゲームをしている時間(秒)を格納


    //作業時間がオーバーしてる
    if ((g_End_ - g_Start_) / 1000 >= GAME_END_TIME) {

        //ゲームオーバーへ画面遷移
        SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
        pSceneManager->ChangeScene(SCENE_ID_GAMEOVER);
    }



}

//=====Enemyの警戒度を管理=====//
//Enemyの色(警戒度)を設定する関数
void GameManagement::GM_EnemySetColor()
{
    //現在の警戒度は青　かつ　見つかった数が最大値に達した
    if (changeMove_ == BLUE && playerSeeCount_ == MAX_FOUND_BLUE)
    {
        //警戒度を黄色に変更
        changeMove_ = YELLOW;

        //見つかっているカウントをリセット
        playerSeeCount_ = 0;

    }

    //現在の警戒度は黄色　かつ　見つかった数が最大値に達した
    if (changeMove_ == YELLOW && playerSeeCount_ == MAX_FOUND_YELLOW)
    {
        //警戒度を赤色に変更
        changeMove_ = RED;

        //見つかっているカウントを∞用数字に
        playerSeeCount_ = INFINITE_COUNT;
    }

    //　1度INFINITE_COUNTになった時に見つかってカウントが増えるのを防ぐため数を固定する
    if (playerSeeCount_ >= INFINITE_COUNT)
        //見つかっているカウントを∞用数字に
        playerSeeCount_ = INFINITE_COUNT;
}

//目を開けたり閉じたりさせる
void GameManagement::GM_OpenClose_Eyes()
{
    //フラグが立っていない
    if (CloseEyesFlg == false)
    {
        //フラグを立たせる
        CloseEyesFlg = true;
    }
    //フラグが立っている
    else
    {
        //フラグを折る
        CloseEyesFlg = false;
    }
}