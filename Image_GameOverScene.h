#pragma once
#include "Engine/GameObject.h"

//ゲームオーバーシーンで使用されるロゴを一つにまとめ管理するクラス
class Image_GameOverScene : public GameObject
{
	//画像のイメージを張り付ける用
	//ゲームオーバーロゴ
	int			gameOverImageHandle_;	
	Transform	gameOverTransform_;

	//コンテニューYNロゴを管理するクラス
	int			continueYNImageHandle_;	
	Transform	continueYNTransform_;

public:
	//コンストラクタ
	Image_GameOverScene(GameObject* parent);

	//デストラクタ
	~Image_GameOverScene();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};