#pragma once
#include "Engine/GameObject.h"



//前方宣言		ポインタだけなら作れる　
//				ステージクラスの中身がわからないといけないものは作れない
class Stage;
class Player;
//class AstarAlgorithm;

//ポイントした経路表示を管理するクラス
//ハッカーモードでクリックした際にクリックした座標までの道のりをマップ上に表示させるクラス
class RoutePoint : public GameObject
{
	int hModel_;    //モデル番号
	Stage* pStage_;
	Player* pPlayer_;

	int pointPosX_;	//ポイントが置かれるポジションXが格納される
	int pointPosZ_;	//ポイントが置かれるポジションZが格納される

	
	/*関数*/

	//点打ち判定処理　
	//引数　なし　
	//戻値　なし
	void Route_PointDraw();


public:

	//コンストラクタ
	RoutePoint(GameObject* parent);

	//デストラクタ
	~RoutePoint();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

};