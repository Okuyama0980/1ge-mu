#include "GameClearScene.h"

#include "Engine/SceneManager.h"
#include "Engine/Input.h"

#include "BackGround_Black.h"
#include "BackGround_Sky.h"
#include "BackGround_RollLeft.h"
#include "BackGround_RollRight.h"

#include "Image_GameClearScene.h"


//コンストラクタ
GameClearScene::GameClearScene(GameObject* parent)
	: GameObject(parent, "GameClearScene")
{
}

//初期化
void GameClearScene::Initialize()
{
	//空
	Instantiate<BackGround_Sky>(this);

	//ゲームオーバーロゴ
	Instantiate<Image_GameClearScene>(this);

}

//更新
void GameClearScene::Update()
{
	//Yキーが押されていたら
	if (Input::IsKey(DIK_Y))
	{
		//画面遷移
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_TITLE);
	}

	//Nキーが押されていたら
	if (Input::IsKey(DIK_N))
	{
		//画面終了
		PostQuitMessage(0);
	}
}

//描画
void GameClearScene::Draw()
{
}

//開放
void GameClearScene::Release()
{
}