#include <time.h>					//時間

#include "Player.h"
#include "Stage.h"					//ステージクラス　レイ用 判定用
#include "RoutePoint.h"

#include "Engine/Model.h"
#include "Engine/Input.h"			//キー　マウス操作のため
#include "Engine/Direct3D.h"
#include "Engine/Camera.h"			//カメラ　レイ用　画面のカメラではない
#include "Engine/SceneManager.h"	//画面遷移
#include "Engine/SphereCollider.h"	//コライダー（衝突範囲）



//コンストラクタ
Player::Player(GameObject * parent)
	:GameObject(parent, "Player"), 
/*変数*/	
		hModel_(-1), changeMode_(1), direction_(1), h_Start_(0), h_End_(0),
/*ポインタ用*/
		pStage_(nullptr), pRoutePoint_(nullptr),
/*フラグ系*/
		pointFlg_(false), hackerFlg_(false),
/*定数*/	
		RADIUS(0.3f), SPEED(0.065f), DASHSPEED(1.3f), LOCATION_X(11.5f), LOCATION_Z(9.5f), SHIFT(0.5), /*AGENTMODE(1), HACKERMODE(2),*/
		DIRECTION_Z(1), DIRECTION_X(2),  DIRECTION_NOTZ(3), DIRECTION_NOTX(4), DIRECTION_OVER(5),
/*ベクトル*/
		DEFO_MOVE{ 0,0,0,0 }
	
	
{
}

//デストラクタ
Player::~Player()
{
}

//初期化
void Player::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Player.fbx");
	assert(hModel_ >= 0);

	//初期位置
	transform_.position_.vecX = LOCATION_X;
	transform_.position_.vecZ = LOCATION_Z;

	//コライダー（衝突範囲）準備
	SphereCollider* collision = new SphereCollider(XMVectorSet(0, 0, 0, 0), 0.3f);
	AddCollider(collision);

	
	pStage_ = (Stage*)FindObject("Stage");	//ステージオブジェクトを探す
	assert(pStage_ != nullptr);				//　pStage_ が null なわけがない！


}

//更新
void Player::Update()
{

	//Tabキーが押されたら操作対象の変更をしたい
	if (Input::IsKeyDown(DIK_TAB))
	{
		//エージェントモードからハッカーモードへ切り替え
		if (changeMode_ == AGENTMODE)
		{ 
			//時間計測に必要なもの
			//押された瞬間からの時間の取得をする
			//時間計測が始まった時のフラグを立てるー＞ハッカーモードのフラグが立っている時のみ時間計測
			//そのフラグが立っているときのみ更新で時間の差分を取り続ける
			//目標の時間になったらフラグを折って更新を止める
			

			//tabが押された時の時間を取得
			h_Start_ = GetTickCount64();

			//切り替え
			changeMode_ = HACKERMODE; 

			
		}
		//ハッカーモードからエージェントモードへ切り替え
		else
		{
			//切り替え
			changeMode_ = AGENTMODE;
		}
	}

	//エージェントモード
	if (changeMode_ == AGENTMODE)
	{
		
		//RoutePointクラスでのフラグ　trueならポイントが打てる
		pointFlg_ = false;

		//Hackerクラスでのフラグ　trueならエージェント(ドローン)を動かせる
		hackerFlg_ = false;

		//エージェントの動き
		Player_AgentMove();

		//end = 0;
	}
	//ハッカーモード
	else
	{
		//RoutePointクラスでのフラグ　trueならポイントが打てる
		pointFlg_ = true;

		//Hackerクラスでのフラグ　trueならエージェント(ドローン)を動かせる
		hackerFlg_ = true;

		//カウントダウンを始める
		//Player_HackerViewMeasureTime();
	}



}



//エージェントキャラの移動
void Player::Player_AgentMove()
{
	XMVECTOR move = DEFO_MOVE;	//移動ベクトル デフォルトの値で毎回初期化

	float playSpeed = SPEED;		//キャラクタの動く最終速度

	XMMATRIX agentCamera;	//エージェントの視点を弄るための行列

	agentCamera = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
	move = XMVector3TransformCoord(move, agentCamera);//　agentCameraに正面のベクトルが入っている


	//正面がZ
	if (direction_ == DIRECTION_Z)
	{
		//Wが押されていたら奥に移動		
		if (Input::IsKey(DIK_W))
		{
			move.vecZ = 1;
		}
		//Sが押されていたら手前に移動	
		if (Input::IsKey(DIK_S))
		{
			move.vecZ = -1;

		}
		//Dが押されていたら右に移動　
		if (Input::IsKey(DIK_D))
		{
			move.vecX = 1;
		}
		//Aが押されていたら左に移動　
		if (Input::IsKey(DIK_A))
		{
			move.vecX = -1;
		}
	}

	//正面が-Z
	if (direction_ == DIRECTION_NOTZ)
	{
		//Wが押されていたら奥に移動		
		if (Input::IsKey(DIK_W))
		{
			move.vecZ = -1;
		}
		//Sが押されていたら手前に移動	
		if (Input::IsKey(DIK_S))
		{
			move.vecZ = 1;

		}
		//Dが押されていたら右に移動　
		if (Input::IsKey(DIK_D))
		{
			move.vecX = -1;
		}
		//Aが押されていたら左に移動　
		if (Input::IsKey(DIK_A))
		{
			move.vecX = 1;
		}
	}

	//正面がX
	if (direction_ == DIRECTION_X)
	{
		//Wが押されていたら奥に移動		
		if (Input::IsKey(DIK_W))
		{
			move.vecX = 1;
		}
		//Sが押されていたら手前に移動	
		if (Input::IsKey(DIK_S))
		{
			move.vecX = -1;

		}
		//Dが押されていたら右に移動　
		if (Input::IsKey(DIK_D))
		{
			move.vecZ = -1;
		}
		//Aが押されていたら左に移動　
		if (Input::IsKey(DIK_A))
		{
			move.vecZ = 1;
		}
	}

	//正面が-X
	if (direction_ == DIRECTION_NOTX)
	{
		//Wが押されていたら奥に移動		
		if (Input::IsKey(DIK_W))
		{
			move.vecX = -1;
		}
		//Sが押されていたら手前に移動	
		if (Input::IsKey(DIK_S))
		{
			move.vecX = 1;

		}
		//Dが押されていたら右に移動　
		if (Input::IsKey(DIK_D))
		{
			move.vecZ = 1;
		}
		//Aが押されていたら左に移動　
		if (Input::IsKey(DIK_A))
		{
			move.vecZ = -1;
		}
	}

	//ダッシュ
	//左シフトを押したときキャラクターが走る		
	if (Input::IsKey(DIK_LSHIFT))
	{
		playSpeed = playSpeed * DASHSPEED;//speedを上げる
	}


	//正面から左を向く
	//←キーが押されたら
	if (Input::IsKeyDown(DIK_LEFT))
	{
		//何らかの処理
		transform_.rotate_.vecY -= 90.0f;

		//現在の正面を切り替える
		direction_--;

		if (direction_ < DIRECTION_Z)
			direction_ = DIRECTION_NOTX;
	}

	//正面から右を向く
	//→キーが押されたら
	if (Input::IsKeyDown(DIK_RIGHT))
	{
		//何らかの処理
		transform_.rotate_.vecY += 90.0f;

		//現在の正面を切り替える
		direction_++;

		if (direction_ > DIRECTION_NOTX)
			direction_ = DIRECTION_Z;
	}

	//振り返る
	//↓キーが押されたら
	if (Input::IsKeyDown(DIK_DOWN))
	{
		//何らかの処理
		transform_.rotate_.vecY += 180.0f;

		//現在の正面を切り替える
		direction_ += 2;

		if (direction_ == DIRECTION_OVER)
			direction_ = DIRECTION_Z;


		if(direction_ == DIRECTION_OVER + 1)
			direction_ = DIRECTION_X;

		
	}

	

	//長さを1にしないと正しい値が出ないので正規化　単位ベクトルにする 進もうとしている方向ベクトル
	move = XMVector3Normalize(move);

	//半径分(0.3)進んでしまうと他の衝突判定も拾ってしまうのでNG
	transform_.position_ += move * playSpeed;//移動 動く速さはSPEEDで調整


	float isMove = XMVector3Length(move).vecX;//長さが0より上ならプレイヤーは動いている XMVector3Length(a).vecX は長さ

	//プレイヤーが動いていれば作動
	if (isMove > 0)
	{
		//正面を向くための関数、いまはEnemyクラスで使う
		//CharTurn(move);//動いている方向に向きを変える

		//壁に当たった時の処理
		Player_HitWall();

		//ゴールに触れた
		Player_HitGoal();
	}


	//エージェントカメラ　一人称視点
	Player_AgentCamera(move, agentCamera);
}






//当たり判定処理　壁
void Player::Player_HitWall()
{
	int checkX, checkZ;		//チェックするための座標用変数

	//円の円周の右、左、奥、手前の判定をつける

	//左の衝突判定
	//intにキャストする　小数点切り捨て　これができるのは mayaでの座標が生きてくる
	checkX = (int)(transform_.position_.vecX - RADIUS);
	checkZ = (int)transform_.position_.vecZ;


	if (pStage_->Stage_IsWall(checkX, checkZ))
	{
		//壁に当たらないポジションに戻す
		transform_.position_.vecX = checkX + 1 + RADIUS;
	}

	//右の衝突判定
	checkX = (int)(transform_.position_.vecX + RADIUS);
	checkZ = (int)transform_.position_.vecZ;

	if (pStage_->Stage_IsWall(checkX, checkZ))
	{
		//壁に当たらないポジションに戻す
		transform_.position_.vecX = checkX - RADIUS;
	}

	//奥の衝突判定
	checkX = (int)transform_.position_.vecX;
	checkZ = (int)(transform_.position_.vecZ + RADIUS);

	if (pStage_->Stage_IsWall(checkX, checkZ))
	{
		//壁に当たらないポジションに戻す
		transform_.position_.vecZ = checkZ - RADIUS;

		
	}

	//手前の衝突判定
	checkX = (int)transform_.position_.vecX;
	checkZ = (int)(transform_.position_.vecZ - RADIUS);

	if (pStage_->Stage_IsWall(checkX, checkZ))
	{
		//壁に当たらないポジションに戻す
		transform_.position_.vecZ = checkZ + 1 + RADIUS;
	}

}

//当たり判定処理　ゴール
void Player::Player_HitGoal()
{
	int checkX, checkZ;		//チェックするための座標用変数

	//ゴールへの衝突判定
	//intにキャストする　小数点切り捨て　
	checkX = (int)(transform_.position_.vecX);
	checkZ = (int)transform_.position_.vecZ;


	if (pStage_->Stage_IsGoal(checkX, checkZ))
	{
		//画面遷移
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_CLEAR);
	}

}

//何かに当たった時
void Player::OnCollision(GameObject* pTarget)
{
	//敵に当たった時
	if (pTarget->GetObjectName() == "WanderEnemy")
	{
		//ゲームオーバーへ画面遷移
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_GAMEOVER);
	}
}


//エージェントモード用のカメラ
void Player::Player_AgentCamera(XMVECTOR move, XMMATRIX camera)
{
	//カメラ
	//transform_.position_がプレイヤーの位置
	//現在地からYを0.5上げる
	Camera::SetTarget(transform_.position_ + XMVectorSet(0, 0.5, 0, 0));

	//カメラベクトル
	XMVECTOR camVec = { 0, 0.5f, -0.1f, 0};

	//cameraに正面のベクトルが入っているので（カメラ）が（正面）からになる
	move = XMVector3TransformCoord(camVec, camera);

	//セット
	Camera::SetPosition(transform_.position_ + move);
}





//描画
void Player::Draw()
{

	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Player::Release()
{
}



