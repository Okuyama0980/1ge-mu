#pragma once
#include <time.h>					//時間

#include "Engine/GameObject.h"

#define AGENTMODE 1		// = 1 AGENTMODE = エージェントモード
#define HACKERMODE 2	// = 2 HACKERMODE = ハッカーモード


class Stage;
class RoutePoint;



//プレイヤーを管理するクラス
class Player : public GameObject
{
	/////変数,定数/////

	Stage* pStage_;
	RoutePoint* pRoutePoint_;

	int hModel_;    //モデル番号


	const float RADIUS ;	// = 0.3; radius = 半径
	const float SPEED;		// = 0.1; speed = 速さ
	const float DASHSPEED;	// = 1.25; Dash speed = 走る速さ
	const float LOCATION_X;	// = 7.5; location_X = 初期位置X
	const float LOCATION_Z;	// = 7.5; location_Y = 初期位置Z
	const float SHIFT;		// = 0.5; shift = ずれる、ずれ


	const XMVECTOR DEFO_MOVE;	// = { 0,0,0,0 }　DEFO_MOVE = デフォルトの移動ベクトル
	
	

	const int DIRECTION_Z;		// = 1; DIRECTION_Z		= Zを向いている
	const int DIRECTION_X;		// = 2; DIRECTION_X		= Xを向いている
	const int DIRECTION_NOTZ;	// = 3; DIRECTION_NOTZ	= -Zを向いている
	const int DIRECTION_NOTX;	// = 4; DIRECTION_NOTX	= -Xを向いている
	const int DIRECTION_OVER;	// = 5; DIRECTION_OVER	= 向いている方向がオーバーしている



	//向きを切り替える用の変数 direction = 向き
	//一番最初はZから始まるので初期値は1から
	//DIRECTION_Z	 = 1 前
	//DIRECTION_X	 = 2 右
	//DIRECTION_NOTZ = 3 後
	//DIRECTION_NOTX = 4 左
	int unsigned direction_;


	//ハッカークラスでモード切り替えするために使いたい
	//ゲームのモードを切り替える用の変数
	//エージェントモード = 1
	//ハッカーモード = 2
	unsigned int changeMode_;


	/*関数*/

	//エージェントキャラの移動
	//引数　なし　
	//戻値　なし
	void Player_AgentMove();

	//当たり判定処理　壁
	//引数　なし　
	//戻値　なし
	void Player_HitWall();


	//当たり判定処理　ゴール
	//引数　なし　
	//戻値　なし
	void Player_HitGoal();


	//エージェント視点のカメラ
	//引数　移動ベクトル　と　行列　
	//戻値　なし
	void Player_AgentCamera(XMVECTOR move , XMMATRIX camera);




	

public:





	//ハッカークラスのアクションで使いたい
	bool pointFlg_;		//ポイントフラグが立っているときに経路ポイントが打てる
	bool hackerFlg_;		//ハッカーフラグが立っているときにハッカーモードのアクションができる

	//ハッカークラスの時間計測で使いたい
	clock_t h_Start_;    // スタート時間 ハッカー側の行動する時間の縛るため
	clock_t h_End_;     // 終了時間　ハッカー側の行動する時間の縛るため

	//コンストラクタ
	Player(GameObject* parent);

	//デストラクタ
	~Player();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//何かに当たった
   //引数　pTarget 当たった相手
	void OnCollision(GameObject* pTarget) override;

	//自分の座標を返す
	//戻値　自分の座標
	XMVECTOR Player_GetPos() { return transform_.position_;}

	//自分のモデルを渡す
	//戻値　自分のモデル
	int Player_GetModelHandle() { return hModel_; }

	//モードをセットする
	//引数　切り替えたいモード
	//エージェントモード = 1
	//ハッカーモード = 2
	void Player_SetChangeMode(int mode) { changeMode_ = mode; }


};