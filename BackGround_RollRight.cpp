#include "BackGround_RollRight.h"
#include "Engine/Image.h"

//コンストラクタ
BackGround_RollRight::BackGround_RollRight(GameObject * parent)
	:GameObject(parent, "BackGround_RollRight"), 
/*変数*/
	rollRightImageHandle_(-1),
/*定数*/
	DEFO_POSX(3.8f), DRAW_POSX(3.79f), OVER_POSX(-2.6f), SPEED(0.01f)
{
}

//デストラクタ
BackGround_RollRight::~BackGround_RollRight()
{
}

//初期化
void BackGround_RollRight::Initialize()
{
	//画像データのロード
	rollRightImageHandle_ = Image::Load("PNG\\BackRoll2.png");
	assert(rollRightImageHandle_ >= 0);
	SetPosition(XMVectorSet(DEFO_POSX, 0.2, 0, 0));


}

//更新
void BackGround_RollRight::Update()
{

	transform_.position_.vecX -= SPEED;//移動

	//再度表示したい
	//Xがある一定値を超えたら
	if (transform_.position_.vecX < OVER_POSX)
	{
		//もう一度描画する位置を定めて描画
		transform_.position_.vecX = DRAW_POSX;
		Image::SetTransform(rollRightImageHandle_, transform_);
		Image::Draw(rollRightImageHandle_);

	}
}

//描画
void BackGround_RollRight::Draw()
{
	Image::SetTransform(rollRightImageHandle_, transform_);
	Image::Draw(rollRightImageHandle_);


}

//開放
void BackGround_RollRight::Release()
{
	rollRightImageHandle_ = -1;
}