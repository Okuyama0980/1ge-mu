#include "Image_TitleScene.h"
#include "SceneTimer.h"

#include "Engine/Image.h"

//コンストラクタ
Image_TitleScene::Image_TitleScene(GameObject* parent)
	:GameObject(parent, "Image_TitleScene"),
/*タイトル*/
	titleImageHandle_(-1), titleTransform_(Transform()),
/*プリーズエンター*/
	pleaseImageHandle_(-1), pleaseTransform_(Transform())
{
}

//デストラクタ
Image_TitleScene::~Image_TitleScene()
{
}

//初期化
void Image_TitleScene::Initialize()
{
	//タイトル
	titleImageHandle_ = Image::Load("PNG\\Title.png");
	assert(titleImageHandle_ >= 0);
	titleTransform_.position_ = XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);
	Image::SetTransform(titleImageHandle_, titleTransform_);

	//プリーズエンター
	pleaseImageHandle_ = Image::Load("PNG\\PleaseEnter!.png");
	assert(pleaseImageHandle_ >= 0);
	pleaseTransform_.position_ = XMVectorSet(0.1f, 0.01f, 0.0f, 0.0f);
	Image::SetTransform(pleaseImageHandle_, pleaseTransform_);
}

//更新
void Image_TitleScene::Update()
{
}

//描画
void Image_TitleScene::Draw()
{
	//タイトル
	Image::Draw(titleImageHandle_);		

	//プリーズエンター 点滅
	auto pressSpaceAlpha = 1.0f - fmod(SceneTimer::GetElapsedSecounds() * 0.65f, 1.f);
	Image::SetAlpha(pleaseImageHandle_, (int)(pressSpaceAlpha * 255.f));
	Image::Draw(pleaseImageHandle_);	
}

//開放
void Image_TitleScene::Release()
{
	//タイトル
	titleImageHandle_ = -1;	

	//プリーズエンター
	pleaseImageHandle_ = -1; 
}