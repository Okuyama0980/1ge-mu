#pragma once
#include "Engine/GameObject.h"

//背景：黒を管理するクラス
class BackGround_Black : public GameObject
{

	//画像のイメージを張り付ける用
	int			blackImageHandle_; //背景黒
	Transform	blackTransform_;	//Transform	backGroundTransform_ = Transform()


public:
	//コンストラクタ
	BackGround_Black(GameObject* parent);

	//デストラクタ
	~BackGround_Black();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};