#include "RoutePoint.h"
#include "Engine/Model.h"

#include "Stage.h"					//ステージクラス　レイ用 判定用
#include "Engine/Input.h"			//キー操作etc　マウス操作のため
#include "Engine/Direct3D.h"		//行列用
#include "Engine/Camera.h"			//カメラ　レイ用　画面のカメラではない
#include "Player.h"					//プレイヤーの位置情報のため


//コンストラクタ
RoutePoint::RoutePoint(GameObject * parent)
	:GameObject(parent, "RoutePoint"), 
/*変数*/	
		hModel_(-1),pointPosX_(0), pointPosZ_(0),
/*ポインタ用*/
		pStage_(nullptr), pPlayer_(nullptr)
/*定数*/	

{
	
	
}

//デストラクタ
RoutePoint::~RoutePoint()
{
}

//初期化
void RoutePoint::Initialize()
{

	transform_.position_.vecX = 9999.0;
	transform_.position_.vecZ = 9999.0;

	//黄色ピンモデルデータのロード
	hModel_ = Model::Load("Pin.fbx");
	assert(hModel_ >= 0);

	pStage_ = (Stage*)FindObject("Stage");	//ステージオブジェクトを探す
	assert(pStage_ != nullptr);				//　pStage_ が null なわけがない！

	pPlayer_ = (Player*)FindObject("Player");	//Playerオブジェクトを探す
	assert(pPlayer_ != nullptr);				//　pPlayer_ が null なわけがない！

}

//更新
void RoutePoint::Update()
{
	//フラグが立っている時
	if(pPlayer_->pointFlg_ == true)
	Route_PointDraw();	
}

//描画
void RoutePoint::Draw()
{
	//描画
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);

}

//開放
void RoutePoint::Release()
{
}



//点打ち判定処理　
void RoutePoint::Route_PointDraw()
{


	int hFloorModel = pStage_->Stage_GetModelFloor();    //床モデル番号を取得

	//-------移動------------
	//前回の位置と現在の位置を引くとどの位動いたかで向きがわかる

	//XMVECTOR move = { 0,0,0,0 }; //前回の位置

	if (Input::IsMouseButtonDown(0))
	{
		//ビューポート行列
		float w = Direct3D::screenWidth_ / 2.0f;
		float h = Direct3D::screenHeight_ / 2.0f;
		XMMATRIX vp =
		{
					w, 0, 0, 0,
					0, -h, 0, 0,
					0, 0, 1, 0,
					w, h, 0, 1
		};

		//各行列の取得
		XMMATRIX prj = Camera::GetProjectionMatrix(); //プロジェクション行列の取得
		XMMATRIX view = Camera::GetViewMatrix();	   //ビュー行列の取得

		//各逆行列の作成
		XMMATRIX invVP = XMMatrixInverse(nullptr, vp);	//ビューポート行列の逆行列
		XMMATRIX invPrj = XMMatrixInverse(nullptr, prj);	//プロジェクション行列の逆行列
		XMMATRIX invView = XMMatrixInverse(nullptr, view);	//ビュー行列の逆行列


		//クリック位置（手前）
		XMVECTOR mousePosFront = Input::GetMousePosition();	//マウスカーソルの位置を入れる
		mousePosFront.vecZ = 0; //Z軸を0にする

		//クリック位置（奥）
		XMVECTOR mousePosBack = Input::GetMousePosition();	//マウスカーソルの位置を入れる
		mousePosBack.vecZ = 1; //Z軸を1にする

		XMMATRIX inverse = invVP * invPrj * invView;	//3つの逆行列を掛け算しておく

		//変換
		mousePosFront = XMVector3TransformCoord(mousePosFront, inverse);	//３つの逆行列(inverse)で変形する。
		mousePosBack = XMVector3TransformCoord(mousePosBack, inverse);	//３つの逆行列(inverse)で変形する。

		float nearX = 0.0f; //近いx
		float nearZ = 0.0f; //近いy
		float miniDist = 9999;	//dist(衝突点までの距離)が遠すぎないようの確認用と初期化
		float isHit = false; //当たったらtrueになるのでUpdate()で繰り返すたびにfalseに戻す

		for (int x = 0; x < pStage_->MAP_LENGTH_X; x++) //横
		{
			for (int z = 0; z < pStage_->MAP_LENGTH_Y; z++) //奥
			{
				Transform trans;

				trans.position_.vecX = x;

				trans.position_.vecZ = z;
				trans.Calclation();

				//レイキャスト
				RayCastData data;
				data.start = mousePosFront;					//レイの発射位置
				data.dir = mousePosBack - mousePosFront;	//レイの方向
				data.dir = XMVector3Normalize(data.dir);	//ベクトルの正規化

				Model::SetTransform(hFloorModel, trans);

				//床にレイを飛ばす
				Model::RayCast(hFloorModel, &data);			//レイを発射

				//当たったかテスト
				if (data.hit)
				{
					//ブレイクポイント用
					//int a = 0;

					isHit = true;	//falseをtrueに
					if (data.dist < miniDist)	//dist(衝突点までの距離)が遠すぎないようの確認
					{
						miniDist = data.dist;

						nearX = x;
						nearZ = z;
					}
				}
			}
		}

		if (isHit)
		{

			//ポイント座標にクリックされた座標を入れる
			pointPosX_ = nearX;
			pointPosZ_ = nearZ;

			//新しいベクトルを作成　表示したい場所のベクトル
			XMVECTOR drawRoutePoint = XMVectorSet(pointPosX_, 0.0f, pointPosZ_, 0.0f);
			//ポジションに表示したい場所をいれる
			transform_.position_ = drawRoutePoint;
			//描画
			Draw(); 



		}
	}

}

