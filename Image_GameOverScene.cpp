#include "Image_GameOverScene.h"
#include "Engine/Image.h"
#include "SceneTimer.h"

//コンストラクタ
Image_GameOverScene::Image_GameOverScene(GameObject* parent)
	:GameObject(parent, "Image_GameOverScene"), 
/*ゲームオーバー*/
	gameOverImageHandle_(-1), gameOverTransform_(Transform()),
/*コンテニューYN*/
	continueYNImageHandle_(-1), continueYNTransform_(Transform())

{
}

//デストラクタ
Image_GameOverScene::~Image_GameOverScene()
{
}

//初期化
void Image_GameOverScene::Initialize()
{
	//ゲームオーバー
	gameOverImageHandle_ = Image::Load("PNG\\GameOver.png");
	assert(gameOverImageHandle_ >= 0);
	gameOverTransform_.position_ = XMVectorSet(0.025f, 0.2f, 0.0f, 0.0f);
	Image::SetTransform(gameOverImageHandle_, gameOverTransform_);

	//コンテニューYN
	continueYNImageHandle_ = Image::Load("PNG\\mContinueYN.png");
	assert(continueYNImageHandle_ >= 0);
	continueYNTransform_.position_ = XMVectorSet(0.0f, -0.2f, 0.0f, 0.0f);
	Image::SetTransform(continueYNImageHandle_, continueYNTransform_);
}

//更新
void Image_GameOverScene::Update()
{
}

//描画
void Image_GameOverScene::Draw()
{
	//ゲームオーバー
	Image::Draw(gameOverImageHandle_);	

	//コンテニューYN  点滅
	auto pressSpaceAlpha = 1.0f - fmod(SceneTimer::GetElapsedSecounds() * 0.65f, 1.f);
	Image::SetAlpha(continueYNImageHandle_, (int)(pressSpaceAlpha * 255.f));
	Image::Draw(continueYNImageHandle_);
}

//開放
void Image_GameOverScene::Release()
{
	//ゲームオーバー
	gameOverImageHandle_ = -1;

	//コンテニューYN
	continueYNImageHandle_ = -1;
}