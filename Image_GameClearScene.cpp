#include "Image_GameClearScene.h"

#include "Engine/Image.h"
#include "SceneTimer.h"

//コンストラクタ
Image_GameClearScene::Image_GameClearScene(GameObject* parent)
	:GameObject(parent, "Image_GameClearScene"),
/*ゲームクリア*/
	gameClearImageHandle_(-1), gameClearTransform_(Transform()),
/*コンテニューYN*/
	continueYNImageHandle_(-1), continueYNTransform_(Transform())

{
}

//デストラクタ
Image_GameClearScene::~Image_GameClearScene()
{
}

//初期化
void Image_GameClearScene::Initialize()
{
	//ゲームクリア
	gameClearImageHandle_ = Image::Load("PNG\\GameClear.png");
	assert(gameClearImageHandle_ >= 0);
	gameClearTransform_.position_ = XMVectorSet(0.025f, 0.2f, 0.0f, 0.0f);
	Image::SetTransform(gameClearImageHandle_, gameClearTransform_);

	//コンテニューYN
	continueYNImageHandle_ = Image::Load("PNG\\sContinueYN.png");
	assert(continueYNImageHandle_ >= 0);
	continueYNTransform_.position_ = XMVectorSet(0.0f, -0.2f, 0.0f, 0.0f);
	Image::SetTransform(continueYNImageHandle_, continueYNTransform_);
}

//更新
void Image_GameClearScene::Update()
{
}

//描画
void Image_GameClearScene::Draw()
{
	//ゲームクリア
	Image::Draw(gameClearImageHandle_);	

	//コンテニューYNロゴ  点滅
	auto pressSpaceAlpha = 1.0f - fmod(SceneTimer::GetElapsedSecounds() * 0.65f, 1.f);
	Image::SetAlpha(continueYNImageHandle_, (int)(pressSpaceAlpha * 255.f));
	Image::Draw(continueYNImageHandle_);
}

//開放
void Image_GameClearScene::Release()
{
	//ゲームクリア
	gameClearImageHandle_ = -1;

	//コンテニューYN
	continueYNImageHandle_ = -1;
}