#pragma once
#include "Engine/GameObject.h"


class GameManagement;

//プレイシーンを管理するクラス
class PlayScene : public GameObject
{
	GameManagement* pGM_;


	bool yellowFlg_OK_;	//イエローフラグに干渉したときのフラグ
	bool redFlg_OK_;		//レッドーフラグに干渉したときのフラグ

public:


	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	PlayScene(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

};