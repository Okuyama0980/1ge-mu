#include "GameOverScene.h"

#include "Engine/SceneManager.h"
#include "Engine/Input.h"

#include "BackGround_Black.h"
#include "BackGround_RollLeft.h"
#include "BackGround_RollRight.h"

#include "Image_GameOverScene.h"

//コンストラクタ
GameOverScene::GameOverScene(GameObject* parent)
	: GameObject(parent, "GameOverScene")
{
}

//初期化
void GameOverScene::Initialize()
{
	//背景　黒
	Instantiate<BackGround_Black>(this);

	//数字　左
	Instantiate<BackGround_RollLeft>(this);

	//数字　右
	Instantiate<BackGround_RollRight>(this);

	//ゲームオーバーロゴ
	Instantiate<Image_GameOverScene>(this);
}

//更新
void GameOverScene::Update()
{
	//Yキーが押されていたら
	if (Input::IsKey(DIK_Y))
	{
		//画面遷移
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_PLAY);
	}

	//Nキーが押されていたら
	if (Input::IsKey(DIK_N))
	{
		//画面遷移
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_TITLE);
	}
}

//描画
void GameOverScene::Draw()
{
}

//開放
void GameOverScene::Release()
{
}