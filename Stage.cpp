#include "Stage.h"
#include "Engine/Model.h"
#include "Engine/CsvReader.h"// csv

//コンストラクタ
Stage::Stage(GameObject * parent)
	:GameObject(parent, "Stage"), 
/*変数*/

/*定数*/
		MAP_LENGTH_X(35), MAP_LENGTH_Y(35)
{
	//モデルの数初期化
	for (int i = 0; i < 3; i++)
	{

		hModel_[i] = -1;
	}

	//------------ csv ---------------
	CsvReader csv;  

	//csv.Load("Map\\StMapAITest2020.csv");
	csv.Load("Map\\STMapC.csv");

	
	for (int i = 0; i < MAP_LENGTH_X; i++)
	{
		for (int j = 0; j < MAP_LENGTH_Y; j++)
		{

			stageA_[i][j] = csv.GetValue(i, j);
			//　　↑
			//table_[0][0] = csv.GetValue(0, 0);
			//table_[0][1] = csv.GetValue(0, 1);
			//table_[0][2] = csv.GetValue(0, 2);

		}
	}
	


	
}

//デストラクタ
Stage::~Stage()
{
}

//初期化
void Stage::Initialize()
{
	//モデルデータのロード
	hModel_[0] = Model::Load("Floor.fbx");
	assert(hModel_[0] >= 0);

	hModel_[1] = Model::Load("Wall.fbx");
	assert(hModel_[1] >= 0);

	hModel_[2] = Model::Load("GoalFoor.fbx");
	assert(hModel_[2] >= 0);




}

//更新
void Stage::Update()
{
}

//描画
void Stage::Draw()
{
	


	for (int x = 0; x < MAP_LENGTH_X; x++) //横
	{
		for (int z = 0; z < MAP_LENGTH_Y; z++) //奥
		{
			//変数を作って配列の中身をいれる
			int type = stageA_[x][z];

			//transformは位置、向き、拡大率のクラス　Xは横 Zは奥
			Transform trans;
			trans.position_.vecX = x;
			trans.position_.vecZ = z;
			Model::SetTransform(hModel_[type], trans);
			Model::Draw(hModel_[type]);
		
		}
	}
}

//開放
void Stage::Release()
{
}

//そこは壁か？
bool Stage::Stage_IsWall(int x, int z)
{
	//if (table_[x][z] == 1)
	//	return true; 1か
	//else
	//	return false;　0か

	return stageA_[x][z] == 1; // table_[x][z]は1と同じか 1なら壁,0なら床
	
}

//そこはゴールか？
bool Stage::Stage_IsGoal(int x, int z)
{
	if (stageA_[x][z] == 2)
	{
		return stageA_[x][z] == 2;
	}

	return 0; //table_[x][z] == 2; // table_[x][z]は2と同じか 1なら壁,0なら床,2ならゴール
}