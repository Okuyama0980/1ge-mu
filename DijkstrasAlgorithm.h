#pragma once
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

//#include "Engine/Model.h"
//#include "Engine/gameObject.h"
#include "Player.h"

using namespace std;

//最小コスト
static int gMinCostResult = INT_MAX;

Player* pPlayer_;

//座標
struct Pos
{
	int y_; //行
	int x_; //列

	Pos() : y_(-2), x_(-2) {};
	Pos(int y, int x) : y_(y), x_(x) {};
};

//マップ構造体
struct Map
{
	Pos connect_; //接続先の座標

	int id_ = 0; //マスのid
	int cost_ = 0; //マスのコスト
	int maxCost_ = INT_MAX; //コスト最大値 int型が取りうる最大値をデフォルトの値にする
};

//配列を探査するときの方向
Pos Direction[4] =
{
	Pos(-1,0), //上
	Pos(1,0),  //下
	Pos(0,1),   //右
	Pos(0,-1) //左
};

//スタートとゴールの座標
Pos START_POS;
Pos GORAL_POS;

//マップのサイズ
const int HEIGHT = 15;	//縦
const int WIDTH = 15;	//横

//マップの構成要素
enum
{
	WALL = -1,
	MITI = 1,
	OKA = 2,
	GAKE = 3
};


//マップ
Map gMap[HEIGHT][WIDTH];

//プロトタイプ宣言
void Init();
int Dijk(int posY, int posX);
void Draw();

//最短経路を表示するために
//map配列に-2をセットする
void Set(int& y, int& x)
{
	int bufY = y;
	int bufX = x;

	int c = gMap[y][x].connect_.y_;
	int b = gMap[y][x].connect_.x_;

	y = gMap[bufY][bufX].connect_.y_;
	x = gMap[bufY][bufX].connect_.x_;

	gMap[c][b].cost_ = -2;
}

//実行
void Run(Pos GORAL_POS, Pos START_POS)
{
	/*Init();*/

	Dijk(START_POS.y_, START_POS.x_);

	//最小コスト
	cout << "最小コスト : " << gMinCostResult << endl;
	cout << endl;

	//最短経路を表示するために-2をセットする
	int x = GORAL_POS.x_;
	int y = GORAL_POS.y_;
	while (true)
	{
		if (y == START_POS.y_ && x == START_POS.x_)
		{
			break;
		}
		Set(y, x);
	}

	Draw();


}

//初期化
void Init()
{
	//スタートとゴールの座標の初期化
	Pos START_POS(0,0);
	Pos GORAL_POS(0,0);

	//pPlayer_ = (Player*)FindObject("Player");	//ステージオブジェクトを探す
	//assert(pPlayer_ != nullptr);				//　pPlayer_ が null なわけがない！

	string strBuf = ""; //一行ずつ格納
	string strConmaBuf = "";  //「,」区切りで格納
	string fileName = "DiMap.csv"; //読み込むファイル

	ifstream ifs(fileName); //ファイルを開く

	//読み込んだcsvを二次元配列に格納するための添え字
	int y = 0; //一次元
	int x = 0; //二次元

	int countId = 0; //マスidを付与する

	//一行ずつstrBufに格納していく
	while (getline(ifs, strBuf))
	{
		x = 0; //初期化

		//「,」区切りごとにデータを読み込むためにsitringstream型にする
		stringstream stream(strBuf);

		//「,」区切りでstrConmaBufに書き込む
		while (getline(stream, strConmaBuf, ','))
		{
			gMap[y][x].cost_ = stoi(strConmaBuf); //数値に変換しつつmapに格納
			gMap[y][x].id_ = countId; //idを付与

			countId++; //idを進める
			x++; //次に進める
		}

		y++; //次に進める
	}

	gMap[START_POS.y_][START_POS.x_].maxCost_ = 0; //スタートの最大コストを0にする
};

//ダイクストラ法
int Dijk(int posY, int posX)
{
	//デバッグ用変数
	static int cou = 0;

	//ゴールにたどり着いたらゴールまでの最大コストを返す
	if (posY == GORAL_POS.y_ && posX == GORAL_POS.x_)
	{
		int minCost = gMap[posY][posX].maxCost_;
		if (minCost < gMinCostResult)
		{
			gMinCostResult = minCost;
		}
	}

	int cost = INT_MAX;

	//自分から見て上下左右を調べる
	for (int i = 0; i < 4; i++)
	{
		//gMapのままだとコードが長くなってしまうので別名に変える
		Map& checkElement = gMap[posY + Direction[i].y_][posX + Direction[i].x_];
		cou++;



		if (checkElement.cost_ != WALL &&
			(gMap[posY][posX].connect_.y_ != (posY + Direction[i].y_) || gMap[posY][posX].connect_.x_ != (posX + Direction[i].x_)) &&
			(START_POS.y_ != (posY + Direction[i].y_) || START_POS.x_ != (posX + Direction[i].x_))
			)
		{

			//調べ先のコストと自分の最大コストを足し合わせる
			//調べ先の最大コストより小さい値なら
			//maxCostをsumCostで更新する
			int sumCost = gMap[posY][posX].maxCost_ + checkElement.cost_;

			//プレイヤーの移動（試し）
			//switch (i)
			//{
			//case 0: pPlayer_->MoveFront();  break;
			//case 1: pPlayer_->MoveBack();	break;
			//case 2: pPlayer_->MoveRight();  break;
			//case 3: pPlayer_->MoveLeft();	break;
			//}

			//プレイヤーの移動（試し）
			//switch (i)
			//{
			//case 0: float PlayerPosFront = pPlayer_->MyPosGet.vecZ; PlayerPosFront += 1;	break;
			//case 1: float PlayerPosBack	 = pPlayer_->MyPosGet.vecZ; PlayerPosBack  -= 1;	break;
			//case 2: float PlayerPosRight = pPlayer_->MyPosGet.vecX; PlayerPosRight += 1;	break;
			//case 3: float PlayerPosLeft  = pPlayer_->MyPosGet.vecX; PlayerPosLeft  -= 1;	break;
			//}

			//cout << "sumCost : " << sumCost << endl;
			//コストが小さいなら更新する
			if (sumCost < gMap[posY + Direction[i].y_][posX + Direction[i].x_].maxCost_)
			{
				gMap[posY + Direction[i].y_][posX + Direction[i].x_].maxCost_ = sumCost;
				gMap[posY + Direction[i].y_][posX + Direction[i].x_].connect_.y_ = posY;
				gMap[posY + Direction[i].y_][posX + Direction[i].x_].connect_.x_ = posX;

	
				//再帰呼び出し
				int c = Dijk(posY + Direction[i].y_, posX + Direction[i].x_);

				if ((START_POS.y_ != (posY + Direction[i].y_) || START_POS.x_ != (posX + Direction[i].x_)) && c < cost)
				{
					
					cost = c;
					
					
				}
			}

		}
	}

	//ゴールにたどり着いたらゴールまでの最大コストを返す
	if (posY == GORAL_POS.y_ && posX == GORAL_POS.x_)
	{
		return cost;//gMap[posY][posX].maxCost_;
	}

	return cost;//gMap[posY][posX].maxCost_; //最大コストを返す
};

//描画
void Draw()
{
	for (int y = 0; y < HEIGHT; y++)
	{
		for (int x = 0; x < WIDTH; x++)
		{
			//スタート地点
			if (START_POS.y_ == y && START_POS.x_ == x)
			{
				cout << "S";
			}
			//ゴール地点
			else if (GORAL_POS.y_ == y && GORAL_POS.x_ == x)
			{
				cout << "G";
			}
			//他の要素
			else
			{
				//ここで調べてみて-2のところが進むべきルートになる
				//なので4方向調べて-2の方向へプレイヤーを動かすといけそう。

				switch (gMap[y][x].cost_)
				{
				case WALL:cout << "#"; break;
				case MITI:cout << " "; break;
				case OKA: cout << "$"; break;
				case GAKE:cout << "@"; break;
				case -2:cout << "*"; break;
				default:
					break;
				}
			}
		}
		cout << endl;
	}
}